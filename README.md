Harmonizer
==========

Lightweight app for creating fast and simple harmony sequences.

You can test it here : [http://harmonizerapi.azurewebsites.net](http://harmonizerapi.azurewebsites.net)

[Mockup on moqups.com](https://moqups.com/Journeyman/LfvhebdP)

[Pivotal tracker](https://www.pivotaltracker.com/n/projects/1336886)
[Trello](https://trello.com/b/UI3xyACw/harmonizer)

### Installation
#### Setting up the API
Create an SQL Server database named 'Harmonizer'.
In visual studio, right click on the `Harmonizer.Infrastructure.Database` project and click 'Publish' to the 'Harmonizer' database

#### Starting the API
Make sure you have a .net core SDK compatible with dotnet core 2.0 (get it [here](https://www.microsoft.com/net/download/core))

In Harmonizer.Api, run : 

`dotnet run`

#### Starting the web app
In Harmonizer.Front, run : 

`yarn install`

Then run : 

`npm start` 

#### Code pens :
- Modern web audio javascript Library : [Howler.js](http://goldfirestudios.com/blog/104/howler.js-Modern-Web-Audio-Javascript-Library)
- [Codepen : Home made cute metronome css animation](http://codepen.io/BlueInt32/pen/EZowNx?editors=110)
- [Codepen : Circle of fifths made with svg](https://codepen.io/BlueInt32/pen/mRppPd)

#### Using React and Redux
- [Dan Abramov course](https://github.com/tayiorbeii/egghead.io_idiomatic_redux_course_notes) and [the code](https://github.com/gaearon/todos)


#### API tech
- Using [Dapper](https://github.com/StackExchange/dapper-dot-net) as orm engine instead of slow and overkill ES Code first
- ... and [how to map n-n relations into dapper](https://www.tritac.com/developers-blog/dapper-net-by-example/)

#### Logo : 
- [Logo designer](http://www.squarespace.com/logo/#N4IgzgpgLlCWB2BzMIBcpECdYBMD6iArrhCqlJoRADQha55jwCGADmgGbMA2kt9-MLABeENACYADP2yCoAT25jUIbrEQALKCAC-tMPIC2AIwD23NKArN4YDqcyHLIAB5oAbO8kA6AOwBWWnk0AEYADn9vdwBOAGZJfwAWEOjo8XCA_QBjHmUAbVjvEJD_STLyirLfakLi0sqG3wBdPRAANx4qZ1xQkPdEuP02xDQQAB5EAAIcZihmAFp4U0J4edwAXgByYv64zenZhdmKWGNCOFN4Lf3rW3tHLbAcpQAKEO9xd38w8XFfMMSYRCkhCn18AEpNgA-MasWYaSYcWDcbhbADEDX2OC2AFkYt5ArEwgSNPNim15rEspIyYVxP5_GSfL5EozxIl5uzOYkwNzqMC_Ky-ey2rFfN5JO5xAA1CIE6n8qL-arvWKJWI1TWxDS-WKTSZZd5farSWJk7xqjVm2IUv4AYTC0QJ1ES4tZiWoYR8SXcNWJgXxhP9wkmON87wDiQJbWBGnEbTCpN8FMT8aZAAlwwSQ3iI9R-tHY_HE_Nk_NUxTgfr0-4IznYoVAizoyWyxWmRobcD0w3s6G-s6C_4NP4Y5I4wmk9Kh_X3M6ftHyxOO6Pwj25_4c4kN_mo8PV-Pi0n9dO9zmwo3PeISaWU8vY_5ZdfNzdMDY7A5DFtbmBuLMIC8TLOmSEaQgA9DCYGIFCIC0IQrDcKYzA4BAmDOIhOQXPAowAHIQAA7pMACaDgANbUJMeGESRmDkZMACqADKsEgCwhjKCAAAizDLCwkwAFIQJcIy0KwqGGDwCCkaMYEzLxzC6LQChiaMSwrCx-GhGUtAaGg0Ruu4F7-LEfwhIC6phLQbSwARABCphuOgrhoNIIDBKgrn4bgUC6agwKuRoEDqFoWmSK0YDDHgrCYBA1kEaMEwHHMiy8Ws2LbH0AyxFihzzMc2BnFh1yTD-9xfpsTy5G8HxfD8fwAkCIJgpCkxgAoShbPM-EQMYpGwFA8ylZ-qAlW-th_lAAH0qwLgUdNLjgq1zwAe8nzfL8_yAsCISxOCADco3vmVI0_hNU3-DNc0XQtS1VattUbQ1227Xt0KwvCiLIqimwYpUWK4oGfo3uSlLUrSHwMkBLJshyXLsry7L8syQqIyKYoSlKsqRP4CpGsqiqWlqNQ6nqBp4yaNTmoT1q2r4DpOoErreO6nretuQMBgzHM5lmkaFoek63kuabdrzOa1oOe5jhOrZ3iLkjVhLm6hr2TZS7LwuVuOXaSD2jY5gOfP7tLR6lqefY4rEO4LqOmsrjGYTrhb26SySB4y8ekzm8rOIXvOz5JnLWsjk-2avkdn7fmNv7_oBAqBCBBLgZB0GKSASIona5gOKMaLxOIxhSixZiYChmBZ4haEqL9FTFw4ZcAOreb5rkl2XABKyGwIQZBhfohDGHAUBKM4Q2OM4jlpIUYSZG5aCJAk3jRCk_S-Ok7i-NEMTZLkaB5CE_ItFZnQcSxSgwKhjFwlkCAjB5tD2PAUAAGLMIYyLuSAOKXFAkCYG-2gH5fQrjnauDQ05DxHk5MeTgnKOUMkaNeXgWTpBnoEOeqAGyRHVP0cQsRUjuHwfjcAy097elXnEPBC9JDpGiAyag5DXSULVGUWhDIj7tBPqMdMzBHCXBEKhM-0BJqYCvswG-SAXIPx_q_d-3BP7fyfn_ABLEM7cBAVXEAaIOA6N0WnMAyxMBZA4hACSyJdBAAA)

#### Fonts : 

- [Google fonts](https://www.google.com/fonts#UsePlace:use/Collection:Roboto+Mono)

#### CSS inspiration :
- [Bulma CSS](http://bulma.io/)
- [Periodic table of elements](http://boag.online/playground/periodictable/)
- [RedditIpsum](http://redditipsum.xyz/)
- Nice JS/CSS3 circular select -> [Codrops](http://tympanus.net/Development/SelectInspiration/index8.html)
- [Top nav bar at stripe.com](https://gitlab.com/)
- [Awwwards](http://www.awwwards.com/websites/)
- [Dribbble](https://dribbble.com/)
- [Drawing CSS arcs to draw a circle of fifths](http://stackoverflow.com/a/13059486/1199535)
- [Drawing SVG arcs](http://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle)
- [Flex explained with](https://medium.freecodecamp.com/an-animated-guide-to-flexbox-d280cf6afc35#.h1z8g2ogs)
- the website http://assetforge.io/


#### Colors : 
- https://dribbble.com/shots/3253445-Chatbot-WIP
- https://dribbble.com/shots/3254596-Astroneer-game-inspired-render
- color management tutorial : [medium article](https://blog.prototypr.io/how-to-use-colors-in-ui-design-16406ec06753#.7b9l9rj7b)

#### Obsolete stuff :
- obsolete [How To update one-many relationships](http://www.entityframeworktutorial.net/EntityFramework4.3/update-one-to-many-entity-using-dbcontext.aspx) (now using Dapper + slapper automapper for database accesses)
- obsolete [How to create a popup (or burger menu or anything similar)](http://jsfiddle.net/92z54z04/1/) (now the project is in ReactJS)
- obsolete [3D flip](http://davidwalsh.name/demo/css-flip.php) generates too complex markup
