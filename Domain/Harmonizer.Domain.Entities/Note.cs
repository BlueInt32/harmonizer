﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonizer.Domain.Entities
{
    /// <summary>
    /// Everything relative to a note. 
    /// This entity/table cannot be in third normal form because flat/sharp notations of enharmony cannot be easily made independant.
    /// </summary>
    public class Note
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string EnharmonicNoteId { get; set; }
    }
}
