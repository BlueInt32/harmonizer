﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonizer.Domain.Entities
{
    public class SearchProgressionQuery
    {
        public SearchProgressionQuery(string term = "")
        {
            Term = term;
            Pagination = new Pagination
            {
                PageNumber = 1,
                PageSize = 10
            };
            Sorting = new Sorting
            {
                Property = ProgressionSortingProperty.Title,
                Direction = "asc"
            };
        }
        public string Term { get; set; }

        public Pagination Pagination { get; set; }
        public Sorting Sorting { get; set; }
    }
}
