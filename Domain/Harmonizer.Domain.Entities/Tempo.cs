﻿
namespace Harmonizer.Domain.Entities
{
	public class Tempo
	{
		public int Id { get; set; }

		public string Name { get; set; }
	}
}
