﻿namespace Harmonizer.Domain.Entities
{
    /// <summary>
    /// Chord put in a progression context. A progression is made of progressionchords ordered by their positions.
    /// </summary>
    public class BarChord
    {
        public int Id { get; set; }

        public int ChordId { get; set; }
    }
}
