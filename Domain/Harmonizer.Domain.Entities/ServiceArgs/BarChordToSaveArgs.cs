﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harmonizer.Domain.Entities.ServiceArgs
{
    public class BarChordToSaveArgs
    {
        public int PositionInBar { get; set; }
        public int ChordId { get; set; }
        public int BarId { get; set; } 
    }
}
