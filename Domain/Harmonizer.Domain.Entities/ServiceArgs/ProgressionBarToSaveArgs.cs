﻿using System.Diagnostics;

namespace Harmonizer.Domain.Entities.ServiceArgs
{
    [DebuggerDisplay("Pos = {PositionInProgression}, Chord = {ChordId}, Seq = {ProgressionId}")]
    public class ProgressionBarToSaveArgs
    {
        public int PositionInProgression { get; set; }
        public int ProgressionId { get; set; }
    }
}
