﻿using System.Collections.Generic;

namespace Harmonizer.Domain.Entities.ServiceArgs
{
    public class ProgressionToSaveArgs
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public IEnumerable<IEnumerable<int>> BarsChordsId { get; set; }
        public int TempoId { get; set; }
    }
}
