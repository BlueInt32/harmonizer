﻿namespace Harmonizer.Domain.Entities
{
    /// <summary>
    /// A note (12 notes possible), a chordType (minor, major, seventh, sixth, etc) and a length in quarter notes.
    /// Careful : this is not directly used as is in a progression. Progression is made of BarChords.
    /// </summary>
    public class Chord
    {
        public int Id { get; set; }
        public string RootNoteId { get; set; }
        public string EnharmonicNoteId { get; set; }
        public string TypeId { get; set; }
        public int ValueId { get; set; }
        public string Notation { get; set; }
    }
}