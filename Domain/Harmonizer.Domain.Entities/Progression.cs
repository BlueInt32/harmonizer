﻿using System.Collections.Generic;

namespace Harmonizer.Domain.Entities
{
    /// <summary>
    /// Essentially series of chords and metadata. This is what can describe harmonically a part of a song : verse, chorus or anything.
    /// </summary>
    public class Progression
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<List<int>> BarsChordsIds { get; set; }
        public int TempoId { get; set; }
    }
}