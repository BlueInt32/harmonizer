﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonizer.Domain.Entities.Exceptions
{
    public class ChordNotFoundException : HarmonizerException
    {
        public ChordNotFoundException(string noteId, int valueId, string typeId)
            : base(
                  errorCode: "CHORD_NOT_FOUND",
                  errorMessage: string.Format($"Chord not found note:{noteId}, value:{valueId}, chordType:{typeId}"))
        {
        }
        public ChordNotFoundException(int chordId)
                    : base(
                          errorCode: "CHORD_NOT_FOUND",
                          errorMessage: string.Format($"Chord not found id:{chordId}"))
        {
        }
        public ChordNotFoundException(List<int> chordsIds)
                    : base(
                          errorCode: "CHORD_NOT_FOUND",
                          errorMessage: string.Format($"Chord not found id:{string.Join(",", chordsIds)}"))
        {
        }
    }
}
