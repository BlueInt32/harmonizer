﻿namespace Harmonizer.Domain.Entities.Exceptions
{
    public class ProgressionNotFoundException : HarmonizerException
    {
        public ProgressionNotFoundException(int progressionId) 
            : base(
                  errorCode: "SEQUENCE_NOT_FOUND",
                  errorMessage: string.Format($"Progression not found {progressionId}"))
        {
        }
    }
}
