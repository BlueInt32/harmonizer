﻿
namespace Harmonizer.Domain.Entities
{
	/// <summary>
	/// Type of chord can be minor, major, dominant seventh, etc. Not note related in any way.
	/// This object has typically a 'color' in musical terms.
	/// </summary>
	public class ChordType
	{
		public string Id { get; set; }

		public string Name { get; set; }

		public string Notation { get; set; }

		public string Description { get; set; }

		public int SpriteOffset { get; set; }

	}
}