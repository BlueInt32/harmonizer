﻿namespace Harmonizer.Domain.Entities
{
    public class Sorting
    {
        public string Direction { get; set; }
        public ProgressionSortingProperty Property { get; set; }
    }

    public enum ProgressionSortingProperty
    {
        Title,
        Rating
    }
}