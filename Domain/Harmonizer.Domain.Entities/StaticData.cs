﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harmonizer.Domain.Entities
{
    public class StaticData
    {
        public List<Tempo> Tempos { get; set; }
        public int DefaultTempoId { get; set; }
        public List<Note> Notes { get; set; }
        public string DefaultNoteId { get; set; }
        public string DefaultEnharmonicNoteId { get; set; }
        public List<ChordType> ChordTypes { get; set; }
        public string DefaultTypeId { get; set; }
        public List<Value> Values { get; set; }
        public int DefaultValueId { get; set; }

        public List<Chord> Chords { get; set; }
    }
}
