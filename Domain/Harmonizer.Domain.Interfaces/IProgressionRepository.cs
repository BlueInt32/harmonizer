﻿using System.Collections.Generic;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Entities.ServiceArgs;

namespace Harmonizer.Domain.Interfaces
{
    public interface IProgressionRepository
    {
        void CreateOrUpdateProgression(ProgressionToSaveArgs progression);
        int CreateProgression(ProgressionToSaveArgs progression);
        void UpdateProgression(ProgressionToSaveArgs progression);
        Progression GetProgression(int progressionId);
        void DeleteProgression(int progressionId);
        List<Progression> FindProgressions(SearchProgressionQuery query);
    }
}
