﻿using Harmonizer.Domain.Entities.ServiceArgs;

namespace Harmonizer.Domain.Interfaces
{
    public interface IProgressionChordRepository
    {
        void CreateProgressionChord(ProgressionBarToSaveArgs progressionChord);
        void DeleteProgressionChord(int progressionChordId);
    }
}
