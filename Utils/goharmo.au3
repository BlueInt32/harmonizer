#include <MsgBoxConstants.au3>

Opt("SendKeyDelay", 0)
Local $iPID = Run("C:\console2\Console.exe")
WinWaitActive("Shell", "")

Send("^r")
Send("+n+p+m{ENTER}")
Sleep (100)
Send("cd /d/_Prog/Projects/Harmonizer/Harmonizer.Front.React{ENTER}")

Send("npm start{ENTER}")

Sleep (200)
Send("^{F1}")
Sleep (100)

Send("^r")
Send("+t+e+s+t{ENTER}")
Sleep (100)
Send("cd /d/_Prog/Projects/Harmonizer/Harmonizer.Front.React{ENTER}")
Send("npm test{ENTER}")

Sleep (200)
Send("^{F1}")
Sleep (100)

Send("^r")
Send("git{ENTER}")
Sleep(100)
Send("cd /d/_Prog/Projects/Harmonizer/Harmonizer.Front.React{ENTER}")
Send("gs{ENTER}")

Local $codePID = Run('"C:\Program Files (x86)\Vim\vim80\gvim.exe"',"", @SW_SHOWMAXIMIZED)
