﻿using System.Collections.Generic;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Entities.ServiceArgs;

namespace Harmonizer.Services.Interfaces
{
    public interface IProgressionService
    {
        Progression GetProgression(int progressionId);
        int CreateProgression(ProgressionToSaveArgs progression);

        void UpdateProgression(ProgressionToSaveArgs progression);
        List<Progression> SearchForProgressions(SearchProgressionQuery query);
    }
}
