﻿using Harmonizer.Domain.Entities;

namespace Harmonizer.Services.Interfaces
{
    public interface IStaticDataService
    {
        StaticData GetStaticData();
        Chord FindChord(string noteId, int valueId, string typeId);
        Chord FindChordById(int id);
    }
}
