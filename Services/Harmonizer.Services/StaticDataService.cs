﻿using System;
using System.Diagnostics;
using System.Linq;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Entities.Exceptions;
using Harmonizer.Domain.Interfaces;
using Harmonizer.Services.Interfaces;

namespace Harmonizer.Services
{
    public class StaticDataService : IStaticDataService
    {
        private readonly IStaticDataRepository _staticDataRepository;
        private StaticData _staticData;

        public StaticDataService(IStaticDataRepository staticDataRepository)
        {
            _staticDataRepository = staticDataRepository;
        }

        public StaticData GetStaticData()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            if (_staticData != null)
                return _staticData;
            var configurations = _staticDataRepository.GetConfigurations();
            _staticData = new StaticData
            {
                ChordTypes = _staticDataRepository.GetChordTypes(),
                Values = _staticDataRepository.GetValues(),
                Notes = _staticDataRepository.GetNotes(),
                Tempos = _staticDataRepository.GetTempos(),
                Chords = _staticDataRepository.GetChords()
            };
            _staticData.DefaultTypeId = configurations.FirstOrDefault(c => c.Key == "DefaultTypeId").Value;
            _staticData.DefaultValueId = Convert.ToInt32(configurations.FirstOrDefault(c => c.Key == "DefaultValueId").Value);
            _staticData.DefaultNoteId = configurations.FirstOrDefault(c => c.Key == "DefaultNoteId").Value;
            _staticData.DefaultTempoId = Convert.ToInt32(configurations.FirstOrDefault(c => c.Key == "DefaultTempoId").Value);

            foreach (var chord in _staticData.Chords)
            {
                ExpandChord(chord, _staticData);
            }
            sw.Stop();
            Debug.WriteLine(sw.ElapsedMilliseconds);
            return _staticData;
        }

        public Chord FindChordById(int id)
        {
            var foundChord = GetStaticData().Chords.FirstOrDefault(c => c.Id == id);
            if (foundChord == null)
            {
                throw new ChordNotFoundException(id);
            }
            return foundChord;
        }

        public Chord FindChord(string noteId, int valueId, string typeId)
        {
            var foundChord = GetStaticData().Chords.FirstOrDefault(
                    c => c.RootNoteId == noteId
                    && c.ValueId == valueId
                    && c.TypeId == typeId);
            if (foundChord == null)
            {
                throw new ChordNotFoundException(noteId, valueId, typeId);
            }
            return foundChord;
        }

        public void ExpandChord(Chord chord, StaticData staticData)
        {
            var rootNote = staticData.Notes.FirstOrDefault(n => n.Id == chord.RootNoteId);
            var chordType = staticData.ChordTypes.FirstOrDefault(t => t.Id == chord.TypeId);

            if (chordType == null || rootNote == null)
            {
                throw new HarmonizerException(
                    "CHORD_INFO_MISSING",
                    string.Format($"Chord info not found for note:{chord.RootNoteId}, value:{chord.ValueId}, chordType:{chord.TypeId}")
                );
            }
            chord.Notation = string.Format($"{rootNote.Name}{chordType.Notation}");
            chord.EnharmonicNoteId = rootNote.EnharmonicNoteId ?? rootNote.Id;
            if (staticData.DefaultNoteId == rootNote.Id)
            {
                staticData.DefaultEnharmonicNoteId = chord.EnharmonicNoteId;
            }
        }
    }
}