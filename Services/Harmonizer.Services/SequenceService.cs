﻿using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Entities.ServiceArgs;
using Harmonizer.Domain.Interfaces;
using Harmonizer.Services.Interfaces;
using System.Collections.Generic;

namespace Harmonizer.Services
{
    public class ProgressionService : IProgressionService
    {
        private readonly IProgressionRepository _progressionRepository;
        private readonly IStaticDataService _staticDataService;

        public ProgressionService(IProgressionRepository progressionRepository, IStaticDataService staticDataService)
        {
            _progressionRepository = progressionRepository;
            _staticDataService = staticDataService;
        }
        public Progression GetProgression(int progressionId)
        {
            return _progressionRepository.GetProgression(progressionId);
        }
        public int CreateProgression(ProgressionToSaveArgs progression)
        {
            return _progressionRepository.CreateProgression(progression);
        }
        public List<Progression> SearchForProgressions(SearchProgressionQuery query)
        {
            return _progressionRepository.FindProgressions(query);
        }

        public void UpdateProgression(ProgressionToSaveArgs progression)
        {
            _progressionRepository.CreateOrUpdateProgression(progression);
        }
    }
}