﻿using System.Collections.Generic;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Entities.Exceptions;
using Harmonizer.Domain.Interfaces;
using Moq;
using NUnit.Framework;

namespace Harmonizer.Services.Test
{
    [TestFixture]
    public class StaticDataServiceTest
    {
        Mock<IStaticDataRepository> _staticDataRepository;

        [SetUp]
        public void TestInitialize()
        {
            _staticDataRepository = new Mock<IStaticDataRepository>();
            _staticDataRepository
                .Setup(r => r.GetChords())
                .Returns(new List<Chord>
                {
                    new Chord {
                        TypeId = "maj7",
                        RootNoteId = "a",
                        ValueId = 1
                    },
                    new Chord {
                        TypeId = "dom7",
                        RootNoteId = "f",
                        ValueId = 2
                    }
                });
            _staticDataRepository
                .Setup(r => r.GetChordTypes())
                .Returns(
                   new List<ChordType>
                   {
                       new ChordType {
                           Id = "maj7",
                           Name = "Major Seventh",
                           Notation = "M7"
                       },
                       new ChordType {
                           Id = "dom7",
                           Name = "Dominant Seventh",
                           Notation = "7"
                       }
                   }

                );
            _staticDataRepository
                .Setup(r => r.GetNotes())
                .Returns(
                new List<Note>
                {
                    new Note {
                        Id = "a",
                        Name = "A"
                    },
                    new Note {
                        Id = "f",
                        Name = "F"
                    }
                }

                );
            _staticDataRepository
                .Setup(r => r.GetValues())
                .Returns(
                    new List<Value>
                    {
                        new Value {
                            Id = 1,
                            Name= "Quarter Note (1)"
                        },
                        new Value {
                            Id = 2,
                            Name= "Half Note (2)"
                        }
                    }
                );
            _staticDataRepository
                .Setup(r => r.GetConfigurations())
                .Returns(
                    new List<Configuration>
                    {
                       new Configuration { Key = "DefaultValueId", Value = "2"  },  
                       new Configuration { Key = "DefaultNoteId", Value = "a"  },  
                       new Configuration { Key = "DefaultTypeId", Value = "maj"  },
                       new Configuration { Key = "DefaultTempoId", Value = "100"  },  
                    }
                );
        }

        [Test]
        public void GetStaticData_ShouldGetAllDataCorrectly()
        {
            // Arrange
            var staticDataService = new StaticDataService(_staticDataRepository.Object);

            // Act
            var staticData = staticDataService.GetStaticData();

            // Assert
            Assert.AreEqual("A", staticData.Notes[0].Name);
            Assert.AreEqual("Major Seventh", staticData.ChordTypes[0].Name);
            Assert.AreEqual("Quarter Note (1)", staticData.Values[0].Name);
            Assert.AreEqual("F", staticData.Notes[1].Name);
            Assert.AreEqual("Dominant Seventh", staticData.ChordTypes[1].Name);
            Assert.AreEqual("Half Note (2)", staticData.Values[1].Name);
        }
    }
}
