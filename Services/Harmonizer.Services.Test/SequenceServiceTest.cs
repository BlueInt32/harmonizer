﻿using System.Collections.Generic;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Interfaces;
using Harmonizer.Services.Interfaces;
using Moq;
using NUnit.Framework;

namespace Harmonizer.Services.Test
{
    [TestFixture]
    public class ProgressionServiceTest
    {
        Mock<IProgressionRepository> _progressionRepository;
        Mock<IStaticDataService> _staticDataService;

        [SetUp]
        public void TestInitialize()
        {
            _progressionRepository = new Mock<IProgressionRepository>();
            _staticDataService = new Mock<IStaticDataService>();

            _progressionRepository
                .Setup(r => r.GetProgression(It.IsAny<int>()))
                .Returns(new Progression
                {
                    Id = 1,
                    BarsChordsIds = new List<List<int>> { new List<int> { 1 } }
                });
            _staticDataService
                .Setup(s => s.FindChordById(1))
                .Returns(new Chord {
                    RootNoteId = "a",
                    TypeId = "maj7",
                    ValueId = 1
                });
        }
        [Test]
        public void GetProgression_ShouldCallTheCorrectDataAccess()
        {
            // Arrange
            var progressionService = new ProgressionService(_progressionRepository.Object, _staticDataService.Object);

            // Act
            var seq = progressionService.GetProgression(1);

            // Assert
            _progressionRepository.Verify(r => r.GetProgression(1), Times.Once);

        }
        
        [Test]
        public void SearchForProgressions_ShouldWorkWithEmptyQuery()
        {
            // Arrange
            var progressionService = new ProgressionService(_progressionRepository.Object, _staticDataService.Object);

            // Act
            var seq = progressionService.SearchForProgressions(null);

            // Assert
            Assert.AreEqual(true, false);
        }
    }
}
