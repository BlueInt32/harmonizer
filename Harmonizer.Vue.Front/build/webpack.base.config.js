const path = require('path')

const config = {

  entry: {
    app: path.resolve(__dirname, '../src/client-entry.js')
  },
  devtool: 'source-map',
  module: {
    rules: [{
      enforce: 'pre',
      test: /(\.js$)|(\.vue$)/,
      loader: 'eslint-loader',
      exclude: /node_modules/
    },
    {
      test: [/\.mp3$/],
      loader: require.resolve('url-loader'),
      options: {
        limit: 10000,
        name: 'static/media/[name].[hash:8].[ext]'
      }
    },
    {
      test: /\.css$/,
      use: [
        { loader: 'css-loader' }
      ]
    },
    {
      test: /\.vue$/,
      loader: 'vue-loader',
      options: {
        css: 'css-loader',
        'scss': 'css-loader|sass-loader'
      }
    },
    {

      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    },
    {
      test: /\.(png|jpg|gif|ico)$/,
      use: [
        {
          loader: 'file-loader',
          options: {}
        }
      ]
    }
    ]
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/',
    filename: 'assets/js/[name].js'
  }
}

module.exports = config
