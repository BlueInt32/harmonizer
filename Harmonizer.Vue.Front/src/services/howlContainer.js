import { Howl } from 'howler'
import Queue from './Queue'
import sounds from '../../assets/sounds/index'
import { isEmpty } from '../utils/common'

const spriteSegmentDuration = 3600
const antiNextTransientTime = 10
// const lengthComputer = (valueId, tempoId) => { return valueId * 60000 / tempoId }

// The main howls array has 12 entries (Howl instances), corresponding to the 12 possible notes in the tempered scale (c, c#, d, d#, ..., a#, b)
// each of these Howls is a soundsprite : a sequence of sounds corresponding to the possibles chords for that note
// The sounds are in the \Harmonizer.Front\Samples folder.
// For a given note, say G, you may want Gm 1 step long, Gsus4 2 steps long, G7 1 step long, GM7 4 steps long, G6 2 steps long, etc
// so for every note (howl), the order is the following :
// - Major triad 1 step, Major triad 2 steps, Major triad 4 steps,
// - Minor triad 1 step, Minor triad 2 steps, Minor triad 4 steps,
// - Major Seventh 1 step, etc...
// Every howls have these chords in the exact same order and timing, given by the
// data in config.js :
// - chordTypesConfig says where a chord types starts in a spritesheet (in ms),
// - durations gives for each duration (given the chordTypesConfig offset) where the durations are situated
export default class HowlsContainer {
  constructor (store) {
    this.store = store
    this.queue = new Queue(true) // true is for 'auto execute' mode
    this.howlsInitialized = false
    this.howls = {}
    this.isPlaying = false
    this.metronome = false
    this.currentlyPlayingHowlId = null
    this.totalProgressionLength = 0
    this.playMode = ''
    this.previousHowlNoteId = null
    this.lastPlayingHowlNoteId = null

    // bindings
    this.initializeHowls = this.initializeHowls.bind(this)
    this.handleNewState = this.handleNewState.bind(this)
    this.mutationCallback = this.mutationCallback.bind(this)
    this.playASound = this.playASound.bind(this)
    this.computeChordMap = this.computeChordMap.bind(this)
    this.playOneChordInProgression = this.playOneChordInProgression.bind(this)
    this.stopPlaying = this.stopPlaying.bind(this)
    this.initializeIfNeeded = this.initializeIfNeeded.bind(this)

    this.store.subscribe(this.mutationCallback)
  }

  // enqueue is the entry point for player new state event callbacks from redux.
  // it garantees handleNewState will execute once by event in the right order
  mutationCallback (actionContext) {
    // filter out actions not taking place in playerModule
    if (actionContext.type === 'playerModule/setPlayingState') {
      // console.log('[howls] mutationCallback', actionContext)
      this.queue.add(this.handleNewState)
    } else if (actionContext.type === 'staticDataModule/setStaticData') {
      // howls cannot be initialized without the static data, so as soon as the corresponding mutation resolved,
      // we initialize the howls
      this.initializeIfNeeded()
    }
  }

  // multiple calls to this method are queued through Queue.js. See mutationCallback() method
  handleNewState () {
    // console.log('[howls] handleNewState')
    try {
      let state = this.store.state
      this.metronome = state.playerModule.metronomeIsActive
      let actionToTake = this.resolveActionToTake(state)
      switch (actionToTake) {
        case 'NONE': return true
        case 'STOP': return this.stopPlaying()
        case 'START': return this.startPlaying(state)
        default: return true
      }
    } catch (e) {
      console.error('error in handleNewState !')
      // console.log('Clearing timer', this.timer)
      clearInterval(this.timer)
      return {
        error: e
      }
    }
  }

  startPlaying (state) {
    this.isPlaying = true
    let playerModule = this.store.state.playerModule
    clearInterval(this.timer)
    this.playMode = playerModule.playMode
    if (playerModule.playMode === 'progression') {
      this.playProgression(playerModule.playerSegments, 100)
    } else if (playerModule.playMode === 'chordInProgression') {
      this.playOneChordInProgression(
        playerModule.segments,
        playerModule.localization.barIndex,
        playerModule.localization.chordIndex,
        100)
    } else if (playerModule.playMode === 'chordOutOfContext') {
      const chordToPlay = playerModule.playerSegments[0][0]
      this.playASound(chordToPlay.enharmonicNoteId, chordToPlay.typeId, chordToPlay.valueId, 100, false, () => {})
    }
    return true
  }

  initializeIfNeeded () {
    if (this.howlsInitialized) {
      return
    }
    console.log('[howls] initialization')

    let staticDataHaveArrived = !isEmpty(this.store.state.staticDataModule.staticData)
    if (staticDataHaveArrived && !this.howlsInitialized) {
      this.initializeHowls(this.store.state.staticDataModule.staticData)
      this.howlsInitialized = true
    }
  }

  resolveActionToTake (state) {
    if (state.playerModule.isPlaying === this.isPlaying) {
      return 'NONE'
    }
    if (!state.playerModule.isPlaying && this.isPlaying) {
      return 'STOP'
    }
    if (state.playerModule.isPlaying && !this.isPlaying) {
      return 'START'
    }
  }

  stopPlaying () {
    this.isPlaying = false
    clearInterval(this.timer)

    if (this.currentlyPlayingHowlId) {
      this.howls[this.currentlyPlayingHowlId].fade(1, 0, 100)
      if (this.soundPlayingTimeoutId) {
        clearTimeout(this.soundPlayingTimeoutId)
      }
    }
    return true
  }

  initializeHowls (staticData) {
    for (let i = 0; i < staticData.notes.length; i++) { // Building howls for each note (12 notes)
      if (staticData.notes[i].enharmonicNoteId) { continue } // we don't create howls for enharmonic notes that's useless
      // console.log('building howl for', props.staticData.notes[i].id);
      let currentSprite = {}
      let currentOffset = 0
      for (let j = 0; j < staticData.chordTypes.length; j++) { // For each chord Type (major triad, minor, major seventh, dominant, etc...
        currentOffset = staticData.chordTypes[j].spriteOffset
        // console.log('type ', j, 'offset ', currentOffset);
        let spriteName = staticData.chordTypes[j].id
        let spriteStartTiming = currentOffset
        let spriteDuration = spriteSegmentDuration - antiNextTransientTime // we substract 10ms to duration because of glitches in sound sprite
        currentSprite[spriteName] = [spriteStartTiming, spriteDuration]
      }

      let noteSoundUrl = sounds[staticData.notes[i].id]
      // console.log('noteSoundUrl', noteSoundUrl);
      this.howls[staticData.notes[i].id] = new Howl(
        {
          src: [noteSoundUrl],
          sprite: currentSprite,
          preload: true
        }
      )
    }
    this.metronomeHowl = new Howl({
      src: [sounds['met']],
      sprite: { tic: [600, 300], tac: [1200, 300] }
    })
    // console.log('finished building howls', this.howls);
  }

  playProgression (bars, tempo) {
    if (!bars.length) {
      return
    }
    this.previousHowlNoteId = null
    let interval = 60000 / tempo
    let step = 1 // step is the tempo count, starts @ 1 because step 0 is made outside of setInterval
    let indexInBar = 1 // indexInBar is just the bar index, in order for the metronome to say tic instead of tac. For now this is supposed to always be equal to step/4
    let barIndex = 0
    this.computeChordMap(bars)

    // start playing first step (setInterval forces us to make first step by hand)
    if (this.metronome) this.metronomeHowl.play('tic')
    this.playOneChordInProgression(bars, 0, 0, tempo)

    let self = this
    this.timer = setInterval(() => {
      if (step >= self.totalProgressionLength) { // has sequence ended ?
        // self.stop();
        return
      }
      if (self.metronome) self.metronomeHowl.play(indexInBar === 0 ? 'tic' : 'tac')
      if (step % 4 === self.chordsStartingSteps[barIndex][indexInBar]) { // is the current step corresponding to the start of a chord in the sequence ?
        let lastBar = barIndex === bars.length - 1
        let lastChordOfBar = indexInBar === self.chordsStartingSteps[barIndex].length - 1
        let lastChordOfProgression = lastBar && lastChordOfBar
        self.playOneChordInProgression(bars, barIndex, indexInBar, tempo, lastChordOfProgression)
        indexInBar = (indexInBar + 1) % 4
      }

      step++
      if (step % 4 === 0) { // detecting start of a bar
        indexInBar = 0
      }
      barIndex = Math.floor(step / 4)
    }, interval)
  }

  // As chords can be 1, 2 or 4 steps-length, we have to set an array containing each chord starting step
  // for instance for Sequence Cm(length:1), G(length:2), Am(length:4), Cm(length 4)
  // chordsStartingSteps will be [0, 1, 3, 7]
  // like Cm starts @ step 0, G starts @ step 1, Am starts @ step 3 (1 + 2), Cm starts @ 7 (1 + 2 + 4)
  computeChordMap (bars) {
    this.chordsStartingSteps = []
    this.reducedChords = []
    this.totalProgressionLength = 0
    let totalBarLength = 0
    for (var i = 0; i < bars.length; i++) {
      this.chordsStartingSteps.push([])
      totalBarLength = 0
      for (var j = 0; j < bars[i].length; j++) {
        this.reducedChords.push(bars[i][j])
        this.chordsStartingSteps[i].push(totalBarLength)
        this.totalProgressionLength += bars[i][j].valueId
        totalBarLength += bars[i][j].valueId
      }
    }
  };

  playOneChordInProgression (bars, barIndex, chordIndex, tempo, lastChordOfProgression) {
    this.playASound(
      bars[barIndex][chordIndex].enharmonicNoteId,
      bars[barIndex][chordIndex].typeId,
      bars[barIndex][chordIndex].valueId,
      tempo,
      lastChordOfProgression,
      () => { this.store.dispatch('playerModule/setPlayingState', { barIndex, chordIndex, isPlaying: false }) }
    )
  };

  playASound (rootNoteId, typeId, valueId, localTempo, lastChordOfProgression, endCallback) {
    let duration = valueId * 60000 / localTempo // chord length in ms
    let fadeOutStart = 0.95 // percent of the sound length when the fadeOut starts
    let fadeOutLength = 0.2 // duration of the fadeOut in percent of the whole duration

    this.howls[rootNoteId].volume(1)
    this.howls[rootNoteId].play(typeId)
    this.lastPlayingHowlNoteId = rootNoteId
    this.currentlyPlayingHowlId = rootNoteId
    this.noteIdPlaying = rootNoteId

    let self = this
    let postFadeStop = true
    self.howls[rootNoteId].once('fade', () => {
      postFadeStop = rootNoteId !== self.lastPlayingHowlNoteId || lastChordOfProgression
      if (postFadeStop) {
        self.howls[rootNoteId].stop() // we need to stop the howl, because fadeOut pauses the sound when ended
        // (which leads to a glitch refade from silence)
        return
      }
      if (lastChordOfProgression) {
        self.stop()
      }
    })
    this.soundPlayingTimeoutId = setTimeout(function () {
      self.howls[rootNoteId].fade(1, 0, duration * fadeOutLength)
      endCallback()
    }, duration * fadeOutStart)
  };

  stop () {
    this.store.dispatch('setPlayingState', { isPlaying: false })
    clearInterval(this.timer)
  };

  render () {
    return (null)
  }
}
