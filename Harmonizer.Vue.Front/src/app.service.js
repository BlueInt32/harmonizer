import axios from 'axios'

const serviceRootUrl = 'http://localhost:5000/api'
axios.interceptors.request.use(function (config) {
  if (typeof window === 'undefined') {
    return config
  }
  const token = window.localStorage.getItem('token')
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
})

const appService = {
  checkApi () {
    return new Promise((resolve, reject) => {
      axios.get(`${serviceRootUrl}/meta`)
        .then(response => {
          resolve('')
        }, response => {
          resolve('DAYUM !')
        })
    })
  },
  getStaticData () {
    return new Promise((resolve, reject) => {
      axios.get(`${serviceRootUrl}/staticdata`)
        .then(response => {
          resolve(response.data)
        }, response => {
          reject(response.error)
        })
    })
  },
  getProgressions () {
    return new Promise((resolve, reject) => {
      axios.get(`${serviceRootUrl}/progressions`)
        .then(response => {
          console.log('[appService] getProgressions', response)
          resolve(response.data)
        }, response => {
          reject(response.error)
        })
    })
  },
  getProgressionDetails (id) {
    return new Promise((resolve, reject) => {
      axios.get(`${serviceRootUrl}/progressions/${id}`)
        .then(response => {
          console.log('[appService] getProgressions', response)
          resolve(response.data)
        }, response => {
          reject(response.error)
        })
    })
  }
}

export default appService
