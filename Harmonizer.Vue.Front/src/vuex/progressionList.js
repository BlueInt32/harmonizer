import appService from '../app.service'
const state = {
  progressions: []
}

const getters = {
  'progressions': state => state.progressions
}

const actions = {
  loadProgressions (context) {
    return new Promise((resolve) => {
      appService.getProgressions().then((progressions) => {
        context.commit('setProgressions', progressions)
        resolve()
      })
    })
  }
}

const mutations = {
  setProgressions (state, progressions) {
    console.log('[mutation] setProgressions', progressions)
    state.progressions = progressions
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
