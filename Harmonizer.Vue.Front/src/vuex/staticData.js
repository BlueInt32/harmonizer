import appService from '../app.service'
const state = {
  staticData: {},
  chordsMap: {}
}

const getters = {
  'staticData': state => state.staticData,
  'chordsMap': state => state.chordsMap
}

const actions = {
  loadStaticData (context) {
    return new Promise((resolve) => {
      appService.getStaticData().then((staticData) => {
        context.commit('setStaticData', staticData)
        resolve(staticData)
      })
    })
  }
}

const mutations = {
  setStaticData (state, staticData) {
    state.staticData = staticData
    state.chordsMap = new Map()
    staticData.chords.forEach(c => {
      state.chordsMap.set(c.id, c)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
