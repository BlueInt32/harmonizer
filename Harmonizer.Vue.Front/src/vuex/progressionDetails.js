import appService from '../app.service'

const state = {
  progressionDetails: {},
  editMode: false
}

const getters = {
  'progressionDetails': state => state.progressionDetails,
  'editMode': state => state.editMode,
  'chordEditionOpen': state => state.chordEditionOpen
}

const actions = {
  loadProgressionDetails (context, progressionId) {
    return new Promise((resolve) => {
      appService.getProgressionDetails(progressionId).then((progressionDetails) => {
        // context.rootState.
        for (let i = 0; i < progressionDetails.barsChordsIds.length; i++) {
          const bar = progressionDetails.barsChordsIds[i]
          for (let j = 0; j < bar.length; j++) {
            const chordId = bar[j]
            let chordDetails = context.rootState.staticDataModule.chordsMap.get(chordId)
            Object.assign(chordDetails, { barIndex: i, chordIndex: j })
            progressionDetails.barsChordsIds[i][j] = chordDetails
          }
        }
        context.commit('setProgressionDetails', progressionDetails)
        resolve(progressionDetails)
      })
    })
  },
  setEditMode (context, newMode) {
    context.commit('setEditMode', newMode)
  }
}

const mutations = {
  setProgressionDetails (state, progressionDetails) {
    state.progressionDetails = progressionDetails
  },
  setEditMode (state, newMode) {
    state.editMode = newMode
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
