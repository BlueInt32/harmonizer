const state = {
  isPlaying: false,
  metronomeIsActive: false,
  playMode: '',
  segments: [],
  localization: {}
}

const getters = {
  'isPlaying': state => state.isPlaying,
  'metronomeIsActive': state => state.metronomeIsActive,
  'playMode': state => state.playModes,
  'segments': state => state.segments,
  'localization': state => state.localization
}

const actions = {
  setMetronomeState (context, isActive) {
    context.commit('setMetronomeState', isActive)
  },
  startPlayingChordInProgression (context, { chordId }) {
    console.log('[action] startPlayingChordInProgression', chordId)
    context.commit('setPlayMode', 'chordInProgression')
    let chord = context.rootState.staticDataModule.chordsMap.get(chordId)
    context.commit('setSegments', [[chord]])
    context.commit('setLocalization', { barIndex: 0, chordIndex: 0 })
    context.commit('setPlayingState', true)
  },
  setPlayingState (context, { isPlaying }) {
    context.commit('setPlayingState', isPlaying)
  }
}

const mutations = {
  setPlayingState (state, isPlaying) {
    state.isPlaying = isPlaying
  },
  setLocalization (state, localization) {
    state.localization = localization
  },
  setPlayMode (state, playMode) {
    state.playMode = playMode
  },
  setSegments (state, segments) {
    state.segments = segments
  },
  setMetronomeState (state, isActive) {
    state.metronomeIsActive = isActive
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
