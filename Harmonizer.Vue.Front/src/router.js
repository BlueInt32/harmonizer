import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './theme/Login.vue'
import NotFound from './theme/NotFound.vue'
import Home from './theme/Home.vue'
import Browser from './theme/ProgressionsList.vue'
import ProgressionDetails from './theme/ProgressionDetails.vue'
import ChordEdition from './theme/ChordEditor.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history', // allows url without the /#/ between host and path
  linkExactActiveClass: 'active',
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) { return savedPosition }
  },
  routes: [
    { path: '/home', component: Home },
    { path: '/browse', component: Browser },
    { path: '/details/:progressionId', name: 'progressionDetails', component: ProgressionDetails },
    { path: '/login', component: Login },
    { path: '/details/:progressionId/:barIndex/:chordIndex', name: 'chordEdition', component: ChordEdition },
    { path: '/', redirect: '/home' },
    { path: '*', component: NotFound }
  ]
})

export default router
