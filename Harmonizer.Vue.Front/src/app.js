import Vue from 'vue'
import store from './vuex/index.js'
import AppLayout from './theme/Layout.vue'
import router from './router'
import HowlsContainer from './services/howlContainer'

const app = new Vue({
  router,
  ...AppLayout,
  store
})

/* eslint-disable no-new */
new HowlsContainer(store)
/* eslint-enable no-new */

export { app, router, store }
