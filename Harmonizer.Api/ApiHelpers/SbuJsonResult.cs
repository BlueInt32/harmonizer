﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Harmonizer.Api
{
    public class SbuJsonResult : SbuJsonResult<object>
    {
        public SbuJsonResult(HttpStatusCode status, object content, JsonSerializerSettings serializerSettings)
            : base(status, content, serializerSettings)
        {
        }
    }

    public class SbuJsonResult<T> : IActionResult where T : new()
    {
        private readonly HttpStatusCode _statusCode;
        private ArraySegment<byte> _segment;

        public SbuJsonResult(HttpStatusCode status, T content, JsonSerializerSettings serializerSettings)
        {
            Content = content;
            SerializerSettings = serializerSettings ?? throw new ArgumentNullException("serializerSettings");
            _statusCode = status;
        }

        /// <summary>Gets the content value to serialize in the entity body.</summary>
        public T Content { get; }

        /// <summary>Gets the serializer settings.</summary>
        public JsonSerializerSettings SerializerSettings { get; }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = (int)_statusCode;

            MediaTypeHeaderValue contentType = new MediaTypeHeaderValue("application/json");
            contentType.CharSet = Encoding.UTF8.WebName;
            context.HttpContext.Response.ContentType = contentType.MediaType;
            _segment = Serialize();
            if (_statusCode != HttpStatusCode.NoContent || Content != null)
            {
                await context.HttpContext.Response.Body.WriteAsync(_segment.Array, _segment.Offset, _segment.Count);
            }

            await context.HttpContext.Response.Body.FlushAsync();
        }

        public async Task Execute(HttpResponse httpResponse)
        {
            httpResponse.StatusCode = (int)_statusCode;
            if (_statusCode == HttpStatusCode.NoContent
                && Content == null)
            {
                return;
            }

            try
            {
                MediaTypeHeaderValue contentType = new MediaTypeHeaderValue("application/json");
                contentType.CharSet = Encoding.UTF8.WebName; httpResponse.ContentType = contentType.MediaType;
                _segment = Serialize();
                httpResponse.Body.Write(_segment.Array, _segment.Offset, _segment.Count);

            }
            finally
            {
                await httpResponse.Body.FlushAsync();
            }
        }

        private ArraySegment<byte> Serialize()
        {
            JsonSerializer serializer = JsonSerializer.Create(SerializerSettings);

            using (MemoryStream stream = new MemoryStream())
            {
                const int DefaultStreamWriterBufferSize = 0x400;
                using (TextWriter textWriter = new StreamWriter(stream, new UTF8Encoding(false),
                    bufferSize: DefaultStreamWriterBufferSize, leaveOpen: true))
                {
                    using (JsonWriter jsonWriter = new JsonTextWriter(textWriter) { CloseOutput = false })
                    {
                        serializer.Serialize(jsonWriter, Content);
                        jsonWriter.Flush();
                    }
                }

                return new ArraySegment<byte>(stream.GetBuffer(), 0, (int)stream.Length);
            }
        }
    }
}