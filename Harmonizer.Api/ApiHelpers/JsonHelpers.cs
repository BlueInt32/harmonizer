﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Harmonizer.Api.ApiHelpers
{
    public static class JsonHelpers
    {
        public static SbuJsonResult<T> SbuJson<T>(this Controller controller, HttpStatusCode status, T content, JsonSerializerSettings serializerSettings)
                   where T : new()
        {
            return new SbuJsonResult<T>(status, content, serializerSettings);
        }

        public static SbuJsonResult<T> SbuJson<T>(this Controller controller, HttpStatusCode status, T content)
            where T : new()
        {
            return new SbuJsonResult<T>(status, content, CamelCasedJsonSerializerSettings);
        }

        public static SbuJsonResult SbuJson(this Controller controller,HttpStatusCode status)
        {
            return new SbuJsonResult(status, null, CamelCasedJsonSerializerSettings);
        }

        public static readonly JsonSerializerSettings CamelCasedJsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            DateFormatString = "s",
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new GuidConverter() }
        };
    }


}
