﻿namespace Harmonizer.Api.Configuration
{
    public class HarmonizerAuthenticationConfiguration
    {
        public string JwtTokenSecret { get; set; }
        public string JwtTokenIssuer { get; set; }
        public string JwtTokenAudience { get; set; }
    }
}
