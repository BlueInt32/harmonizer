﻿using Harmonizer.Api.Model;
using Harmonizer.Services.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Harmonizer.Api.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStaticDataService _staticDataService;
        public HomeController(IStaticDataService staticDataService)
        {
            _staticDataService = staticDataService;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var viewModel = new UIStaticConfigurationModel
            {
                StaticData = JsonConvert.SerializeObject(_staticDataService.GetStaticData(), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                })
            };
            return View(viewModel);
        }
    }
}
