﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Harmonizer.Api.Configuration;
using Harmonizer.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Harmonizer.Api.Controllers
{
    [Route("api/token")]
    public class TokensController : BaseController
    {
        private readonly HarmonizerAuthenticationConfiguration _config;

        public TokensController(IOptions<HarmonizerAuthenticationConfiguration> subOptionsAccessor)
        {
            _config = subOptionsAccessor.Value;
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> GenerateToken([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var user = await _userManager.FindByEmailAsync(model.Email);

                if (CheckUserExists())
                {
                    var user = new HarmonizerUser { Email = model.Email };
                    //var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                    if (CheckPasswordIsRight())
                    {
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        };

                        var key = new SymmetricSecurityKey
                        (
                            Encoding.UTF8.GetBytes(_config.JwtTokenSecret)
                        );
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        var token = new JwtSecurityToken
                        (
                            issuer: _config.JwtTokenIssuer,
                            audience: _config.JwtTokenIssuer,
                            claims: claims,
                            expires: DateTime.Now.AddMinutes(30),
                            signingCredentials: creds
                        );

                        return SbuJson<dynamic>(
                            System.Net.HttpStatusCode.OK,
                            new
                            {
                                token = new JwtSecurityTokenHandler().WriteToken(token)
                            }
                        );
                    }
                }
            }

            return BadRequest("Could not create token");
        }
        private bool CheckUserExists()
        {
            return true;
        }
        private bool CheckPasswordIsRight()
        {
            return true;
        }

        internal class HarmonizerUser
        {
            public string Email { get; set; }
        }
    }
}
