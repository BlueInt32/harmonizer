﻿using Harmonizer.Api.Model;
using Harmonizer.Domain.Entities;
using Harmonizer.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Harmonizer.Api.Controllers
{
    [Route("api/progressions")]
    public class ProgressionsController : BaseController
    {
        private readonly IProgressionService _progressionService;

        public ProgressionsController(IProgressionService progressionService)
        {
            _progressionService = progressionService;
        }

        [HttpPost("")]
        public IActionResult SaveProgression([FromBody]ProgressionViewModel model)
        {
            var saveProgressionArgs = model.ToServiceArgs();
            var progressionId = _progressionService.CreateProgression(saveProgressionArgs);
            return SbuJson<int>(HttpStatusCode.OK, progressionId);
        }

        [HttpPut("{progressionId}")]
        public IActionResult ReplaceProgression([FromRoute]int progressionId, [FromBody]ProgressionViewModel model)
        {
            var saveProgressionArgs = model.ToServiceArgs();
            saveProgressionArgs.Id = progressionId;
            _progressionService.UpdateProgression(saveProgressionArgs);
            return SbuJson(HttpStatusCode.NoContent);
        }

        [HttpGet("")]
        public IActionResult GetProgressions()
        {
            var result = _progressionService.SearchForProgressions(new SearchProgressionQuery());
            return SbuJson(HttpStatusCode.OK, result);
        }

        [HttpGet("{id}")]
        public IActionResult GetProgression(int id)
        {
            return SbuJson(HttpStatusCode.OK, _progressionService.GetProgression(id));
        }
    }
}