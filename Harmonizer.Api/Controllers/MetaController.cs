﻿using Harmonizer.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace vStack.Api.Controllers
{
    [Route("api/meta")]
    public class MetaController : BaseController
    {
        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            return SbuJson(HttpStatusCode.OK);
        }
    }
}
