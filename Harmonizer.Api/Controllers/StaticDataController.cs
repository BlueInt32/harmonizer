﻿using Harmonizer.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Harmonizer.Api.Controllers
{
    [Route("api/staticdata")]
    //[Authorize]
    public class StaticDataController : BaseController
    {
        private readonly IStaticDataService _staticDataService;
        public StaticDataController(IStaticDataService staticDataService)
        {
            _staticDataService = staticDataService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            var staticData = _staticDataService.GetStaticData();
            return SbuJson(staticData);
        }
    }
}
