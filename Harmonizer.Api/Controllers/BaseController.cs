﻿using System.Collections.Generic;
using System.Net;
using Harmonizer.Api.ApiHelpers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Harmonizer.Api.Controllers
{
    public class BaseController : Controller
    {
        public SbuJsonResult<T> SbuJson<T>(HttpStatusCode status, T content, JsonSerializerSettings serializerSettings)
                           where T : new()
        {
            return new SbuJsonResult<T>(status, content, serializerSettings);
        }

        public SbuJsonResult<T> SbuJson<T>(HttpStatusCode status, T content)
            where T : new()
        {
            return new SbuJsonResult<T>(status, content, CamelCasedJsonSerializerSettings);
        }

        public SbuJsonResult<T> SbuJson<T>(T content)
            where T : new()
        {
            return new SbuJsonResult<T>(HttpStatusCode.OK, content, CamelCasedJsonSerializerSettings);
        }

        public SbuJsonResult SbuJson(HttpStatusCode status)
        {
            return new SbuJsonResult(status, null, CamelCasedJsonSerializerSettings);
        }

        public readonly JsonSerializerSettings CamelCasedJsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            DateFormatString = "s",
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new GuidConverter() }
        };

    }
}
