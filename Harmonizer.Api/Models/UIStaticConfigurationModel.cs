﻿using Harmonizer.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harmonizer.Api.Model
{
    public class UIStaticConfigurationModel
    {
        public string StaticData { get; set; }
    }
}
