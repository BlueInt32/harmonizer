﻿using System;
namespace Harmonizer.Api.Model
{
	public class ChordDescriptorViewModel
	{
		public int ProgressionChordId { get; set; }
		public string NoteId { get; set; }
		public int ValueId { get; set; }
		public string ChordTypeId { get; set; }
		public string Notation { get; set; }
	}
}