﻿using System.Collections.Generic;
using Harmonizer.Domain.Entities.ServiceArgs;

namespace Harmonizer.Api.Model
{
    public class ProgressionViewModel
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public int Tempo { get; set; }
        public List<List<int>> Chords { get; set; }

        public ProgressionToSaveArgs ToServiceArgs()
        {
            var progressionToSaveArgs = new ProgressionToSaveArgs();
            progressionToSaveArgs.Id = Id;
            progressionToSaveArgs.Title = Title;
            progressionToSaveArgs.Description = Description;
            progressionToSaveArgs.TempoId = Tempo;
            progressionToSaveArgs.BarsChordsId = Chords;
            return progressionToSaveArgs;
        }
    }
}