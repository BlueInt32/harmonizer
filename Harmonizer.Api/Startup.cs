﻿using System.Text;
using Harmonizer.Api.Configuration;
using Harmonizer.Api.ExceptionFilters;
using Harmonizer.Domain.Interfaces;
using Harmonizer.Repository;
using Harmonizer.Services;
using Harmonizer.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Alt
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            var x = Configuration["AuthenticationSettings:jwtTokenSecret"];
            var y = Configuration["AuthenticationSettings:jwtTokenIssuer"];
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<HarmonizerAuthenticationConfiguration>(Configuration.GetSection("Authentication"));
            services.AddMvc(options => { });
            services.AddScoped<IProgressionService, ProgressionService>();
            services.AddScoped<IStaticDataService, StaticDataService>();
            services.AddScoped<IProgressionRepository>((_) =>
            {
                return new ProgressionRepository(Configuration.GetConnectionString("Harmonizer"));
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(
                cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;

                    cfg.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidIssuer = Configuration["Authentication:jwtTokenIssuer"],
                        ValidAudience = Configuration["Authentication:jwtTokenIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:jwtTokenSecret"]))
                    };

                });
            services.AddScoped<IStaticDataRepository>((_) =>
            {
                return new StaticDataRepository(Configuration.GetConnectionString("Harmonizer"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                       .AllowAnyHeader()
                       .AllowAnyMethod()
            );
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseAuthentication();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseMvc();

            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
