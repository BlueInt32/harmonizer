Opt("SendKeyDelay", 2)
#include <MsgBoxConstants.au3>
$microDelay = 150
$rootPath = IniRead("harmonizer.ini", "config", "ROOT", "default")
$commands = IniReadSection ("harmonizer.ini", "cdAndCommands")
Run("C:\ConsoleZ\tools\Console.exe")
WinWaitActive("bash", "") 
;MsgBox($MB_SYSTEMMODAL, "Title", $commands[0][0] & " commands")
CreateTabs($commands)
Sleep($microDelay)
Send("^{TAB}")
LaunchCommands($commands)

Func CreateTabs($commands)
    $first = true

    For $i=1 To Int($commands[0][0]) Step 1
	$tabName = $commands[$i][0]
        CreateTab($tabName, $first)
	$first = false
    Next
EndFunc

Func LaunchCommands($commandsArray)
    $cd = ""
    $cmd = ""
    $actuallyLaunch = true
    For $i=1 To Int($commandsArray[0][0]) Step 1 ;iterate through all the tabs
	$split = StringSplit($commandsArray[$i][1], "|")
	$count = $split[0] 
    $cd = $split[1]
    $cmd = $split[2]
	If $count = 2 Then
        $actuallyLaunch = true
    ElseIf $count = 3 Then
        If $split[3] = "regular" Then
            $actuallyLaunch = true
        ElseIf $split[3] = "noLaunch" Then
            $actuallyLaunch = false 
        EndIf 
	EndIf
	;MsgBox($MB_SYSTEMMODAL, "Title", $cd & " " & $cmd)
    Launch($cd, $cmd, $actuallyLaunch)
    Next
EndFunc

Func CreateTab($tabName, $first)
   If Not ($first) Then
     Send("^{F1}")
     Sleep ($microDelay)
   EndIf
   Send("^r")
   Sleep ($microDelay)
   WriteFast($tabName)
   Sleep ($microDelay+50)
   Send("{ENTER}")
   Sleep ($microDelay)
EndFunc

Func WriteFast($text)
    ClipPut($text)
    Sleep(50)
    Send("^v")
EndFunc

Func Launch($cdCmd, $cmd, $actuallyLaunch)
    If $cdCmd <> "" Then
        WriteFast("cd " & $rootPath & $cdCmd)
        Send("{ENTER}")
    EndIf
    WriteFast($cmd)
    If $actuallyLaunch = "regular" Then 
        Send("{ENTER}")
    EndIf
    Sleep($microDelay)
    Send("^{TAB}")
EndFunc
