﻿CREATE TABLE [dbo].[ProgressionBars] (
    [Id]                 INT IDENTITY (1, 1) NOT NULL,
    [PositionInProgression] INT NOT NULL,
    [ProgressionId]         INT NOT NULL,
    CONSTRAINT [PK_dbo.ProgressionBars] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ProgressionBars_dbo.Progressions_ProgressionId] FOREIGN KEY ([ProgressionId]) REFERENCES [dbo].[Progressions] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ProgressionId]
    ON [dbo].[ProgressionBars]([ProgressionId] ASC);

