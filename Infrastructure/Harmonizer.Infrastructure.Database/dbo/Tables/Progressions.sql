﻿CREATE TABLE [dbo].[Progressions] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Title]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (255) NULL,
    [TempoId]     INT            NOT NULL,
    CONSTRAINT [PK_dbo.Progressions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Progressions_dbo.Tempos_TempoId] FOREIGN KEY ([TempoId]) REFERENCES [dbo].[Tempos] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_TempoId]
    ON [dbo].[Progressions]([TempoId] ASC);

