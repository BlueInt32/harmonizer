﻿CREATE TABLE [dbo].[BarChords]
(
	[Id]                 INT IDENTITY (1, 1) NOT NULL, 
    [PositionInBar] INT NOT NULL, 
    [ChordId] INT NOT NULL
    CONSTRAINT [PK_dbo.BarChords] PRIMARY KEY CLUSTERED ([Id] ASC),
    [BarId] INT NOT NULL, 
    CONSTRAINT [FK_dbo.BarChords_dbo.Chords_ChordId] FOREIGN KEY ([ChordId]) REFERENCES [dbo].[Chords] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.BarChords_dbo.ProgressionBars_Id] FOREIGN KEY ([BarId]) REFERENCES [dbo].[ProgressionBars] ([Id]) ON DELETE CASCADE,
)
