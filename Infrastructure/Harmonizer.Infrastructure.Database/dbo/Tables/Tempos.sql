﻿CREATE TABLE [dbo].[Tempos] (
    [Id]        INT           NOT NULL,
    [Name]      NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_dbo.Tempos] PRIMARY KEY CLUSTERED ([Id] ASC)
);

