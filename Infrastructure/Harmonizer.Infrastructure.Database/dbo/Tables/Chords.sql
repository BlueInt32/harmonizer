﻿CREATE TABLE [dbo].[Chords] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [RootNoteId]  NVARCHAR (2) NOT NULL,
    [TypeId] NVARCHAR (5) NOT NULL,
    [ValueId]  INT          NOT NULL,
    CONSTRAINT [PK_dbo.Chords] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Chords_dbo.ChordTypes_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[ChordTypes] ([Id]),
    CONSTRAINT [FK_dbo.Chords_dbo.Values_ValueId] FOREIGN KEY ([ValueId]) REFERENCES [dbo].[Values] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.Chords_dbo.Notes_RootNoteId] FOREIGN KEY ([RootNoteId]) REFERENCES [dbo].[Notes] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_RootNoteId]
    ON [dbo].[Chords]([RootNoteId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TypeId]
    ON [dbo].[Chords]([TypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ValueId]
    ON [dbo].[Chords]([ValueId] ASC);

