﻿CREATE TABLE [dbo].[Notes] (
    [Id]        NVARCHAR(2) NOT NULL,
    [Name]      NVARCHAR(2) NOT NULL,
    [Order] SMALLINT          NOT NULL,
    [EnharmonicNoteId] NVARCHAR(2) REFERENCES Notes (Id)
    CONSTRAINT [PK_dbo.Notes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

