﻿CREATE TABLE [dbo].[Values] (
    [Id]        INT           NOT NULL,
    [Name]      NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_dbo.Values] PRIMARY KEY CLUSTERED ([Id] ASC)
);

