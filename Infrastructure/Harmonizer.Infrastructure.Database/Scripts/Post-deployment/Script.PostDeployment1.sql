﻿/*
Post-Deployment Script Template                            
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.        
 Use SQLCMD syntax to include a file in the post-deployment script.            
 Example:      :r .\myfile.sql                                
 Use SQLCMD syntax to reference a variable in the post-deployment script.        
 Example:      :setvar TableName MyTable                            
               SELECT * FROM [$(TableName)]                    
--------------------------------------------------------------------------------------
*/


IF (EXISTS(SELECT * FROM [dbo].[Chords]))
BEGIN
    DELETE FROM [dbo].[Chords]
END
GO

IF (EXISTS(SELECT * FROM [dbo].[ChordTypes]))
BEGIN
    DELETE FROM [dbo].[ChordTypes]
END


IF (EXISTS(SELECT * FROM [dbo].[Tempos]))
BEGIN
    DELETE FROM [dbo].[Tempos]
END
GO


IF (EXISTS(SELECT * FROM [dbo].[Values]))
BEGIN
    DELETE FROM [dbo].[Values]
END
GO
IF (EXISTS(SELECT * FROM [dbo].[Notes]))
BEGIN
    DELETE FROM [dbo].[Notes]
END
GO


IF (EXISTS(SELECT * FROM [dbo].[Configurations]))
BEGIN
    DELETE FROM [dbo].[Configurations]
END
GO

GO
INSERT [dbo].[ChordTypes] ([Id], [Name], [Notation], [Description], [Order], [SpriteOffset]) VALUES (N'maj', N'Major Triad', N'', N'', 1, 0)
GO
INSERT [dbo].[ChordTypes] ([Id], [Name], [Notation], [Description], [Order], [SpriteOffset]) VALUES (N'min', N'Minor Triad', N'm', N'', 2, 3600)
GO
INSERT [dbo].[ChordTypes] ([Id], [Name], [Notation], [Description], [Order], [SpriteOffset]) VALUES (N'dom7', N'Dominant Seventh', N'7', N'', 3, 7200)
GO
INSERT [dbo].[ChordTypes] ([Id], [Name], [Notation], [Description], [Order], [SpriteOffset]) VALUES (N'maj7', N'Major Seventh', N'M7', N'', 4, 10800)
GO



IF (EXISTS(SELECT * FROM [dbo].[Values]))
BEGIN
    DELETE FROM [dbo].[Values]
END
GO
INSERT [dbo].[Values] ([Id], [Name]) VALUES (1, N'Quarter Note (1)')
GO
INSERT [dbo].[Values] ([Id], [Name]) VALUES (2, N'Half Note (2)')
GO
INSERT [dbo].[Values] ([Id], [Name]) VALUES (4, N'Whole Note (4)')
GO

IF (EXISTS(SELECT * FROM [dbo].[Notes]))
BEGIN
    DELETE FROM [dbo].[Notes]
END
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'a', N'A', 1, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'bf', N'B♭', 3, NULL)
GO
-- we place flat notes before so that self foreign keys exist for sharp notes insertions ;)
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'as', N'A♯', 2, N'bf')
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'b', N'B', 4, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'c', N'C', 5, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'df', N'D♭', 7, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'cs', N'C♯', 6, N'df')
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'd', N'D', 8, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'ef', N'E♭', 10, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'ds', N'D♯', 9, N'ef')
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'e', N'E', 11, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'f', N'F', 12, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'gf', N'G♭', 14, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'fs', N'F♯', 13, N'gf')
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'g', N'G', 15, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'af', N'A♭', 17, NULL)
GO
INSERT [dbo].[Notes] ([Id], [Name], [Order], [EnharmonicNoteId]) VALUES (N'gs', N'G♯', 16, N'af')
GO




SET IDENTITY_INSERT [dbo].[Chords] ON 

INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (1, N'a', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (2, N'a', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (3, N'a', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (4, N'af', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (5, N'af', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (6, N'af', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (7, N'as', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (8, N'as', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (9, N'as', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (10, N'b', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (11, N'b', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (12, N'b', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (13, N'bf', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (14, N'bf', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (15, N'bf', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (16, N'c', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (17, N'c', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (18, N'c', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (19, N'cs', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (20, N'cs', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (21, N'cs', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (22, N'd', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (23, N'd', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (24, N'd', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (25, N'df', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (26, N'df', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (27, N'df', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (28, N'ds', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (29, N'ds', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (30, N'ds', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (31, N'e', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (32, N'e', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (33, N'e', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (34, N'ef', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (35, N'ef', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (36, N'ef', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (37, N'f', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (38, N'f', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (39, N'f', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (40, N'fs', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (41, N'fs', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (42, N'fs', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (43, N'g', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (44, N'g', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (45, N'g', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (46, N'gf', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (47, N'gf', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (48, N'gf', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (49, N'gs', N'dom7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (50, N'gs', N'dom7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (51, N'gs', N'dom7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (52, N'a', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (53, N'a', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (54, N'a', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (55, N'af', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (56, N'af', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (57, N'af', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (58, N'as', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (59, N'as', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (60, N'as', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (61, N'b', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (62, N'b', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (63, N'b', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (64, N'bf', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (65, N'bf', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (66, N'bf', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (67, N'c', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (68, N'c', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (69, N'c', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (70, N'cs', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (71, N'cs', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (72, N'cs', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (73, N'd', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (74, N'd', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (75, N'd', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (76, N'df', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (77, N'df', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (78, N'df', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (79, N'ds', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (80, N'ds', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (81, N'ds', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (82, N'e', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (83, N'e', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (84, N'e', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (85, N'ef', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (86, N'ef', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (87, N'ef', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (88, N'f', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (89, N'f', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (90, N'f', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (91, N'fs', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (92, N'fs', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (93, N'fs', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (94, N'g', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (95, N'g', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (96, N'g', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (97, N'gf', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (98, N'gf', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (99, N'gf', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (100, N'gs', N'maj', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (101, N'gs', N'maj', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (102, N'gs', N'maj', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (103, N'a', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (104, N'a', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (105, N'a', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (106, N'af', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (107, N'af', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (108, N'af', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (109, N'as', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (110, N'as', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (111, N'as', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (112, N'b', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (113, N'b', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (114, N'b', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (115, N'bf', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (116, N'bf', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (117, N'bf', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (118, N'c', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (119, N'c', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (120, N'c', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (121, N'cs', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (122, N'cs', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (123, N'cs', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (124, N'd', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (125, N'd', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (126, N'd', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (127, N'df', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (128, N'df', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (129, N'df', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (130, N'ds', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (131, N'ds', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (132, N'ds', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (133, N'e', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (134, N'e', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (135, N'e', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (136, N'ef', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (137, N'ef', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (138, N'ef', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (139, N'f', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (140, N'f', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (141, N'f', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (142, N'fs', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (143, N'fs', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (144, N'fs', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (145, N'g', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (146, N'g', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (147, N'g', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (148, N'gf', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (149, N'gf', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (150, N'gf', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (151, N'gs', N'maj7', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (152, N'gs', N'maj7', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (153, N'gs', N'maj7', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (154, N'a', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (155, N'a', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (156, N'a', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (157, N'af', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (158, N'af', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (159, N'af', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (160, N'as', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (161, N'as', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (162, N'as', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (163, N'b', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (164, N'b', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (165, N'b', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (166, N'bf', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (167, N'bf', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (168, N'bf', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (169, N'c', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (170, N'c', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (171, N'c', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (172, N'cs', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (173, N'cs', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (174, N'cs', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (175, N'd', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (176, N'd', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (177, N'd', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (178, N'df', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (179, N'df', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (180, N'df', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (181, N'ds', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (182, N'ds', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (183, N'ds', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (184, N'e', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (185, N'e', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (186, N'e', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (187, N'ef', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (188, N'ef', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (189, N'ef', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (190, N'f', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (191, N'f', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (192, N'f', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (193, N'fs', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (194, N'fs', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (195, N'fs', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (196, N'g', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (197, N'g', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (198, N'g', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (199, N'gf', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (200, N'gf', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (201, N'gf', N'min', 4)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (202, N'gs', N'min', 1)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (203, N'gs', N'min', 2)
GO
INSERT [dbo].[Chords] ([Id], [RootNoteId], [TypeId], [ValueId]) VALUES (204, N'gs', N'min', 4)
GO
SET IDENTITY_INSERT [dbo].[Chords] OFF
GO

INSERT [dbo].[Tempos] ([Id], [Name]) VALUES (70, N'slow (70bpm)')
GO
INSERT [dbo].[Tempos] ([Id], [Name]) VALUES (85, N'85 bpm')
GO
INSERT [dbo].[Tempos] ([Id], [Name]) VALUES (100, N'100 bpm')
GO
INSERT [dbo].[Tempos] ([Id], [Name]) VALUES (115, N'115 bpm')
GO
INSERT [dbo].[Tempos] ([Id], [Name]) VALUES (130, N'fast (130bpm)')
GO

INSERT [dbo].[Configurations] ([Key], [Value]) VALUES (N'DefaultValueId', N'2')
GO
INSERT [dbo].[Configurations] ([Key], [Value]) VALUES (N'DefaultNoteId', N'c')
GO
INSERT [dbo].[Configurations] ([Key], [Value]) VALUES (N'DefaultTempoId', N'100')
GO
INSERT [dbo].[Configurations] ([Key], [Value]) VALUES (N'DefaultTypeId', N'maj')
GO
