import initialState from './initialState';
import { createReducer } from './utils';
import { updateObject, deepCloneArray, findChordFromChange } from '../common';

export default createReducer(
  initialState.progressionContext,
  {
    'LOAD_PROGRESSION_DETAILS_SUCCESS': loadProgressionDetailSuccess,
    'EMPTY_PROGRESSION_DETAILS': emptyProgressionDetails,
    'CLEAR_CURRENT_PROGRESSION': clearCurrentProgression,
    'SET_CHORD_IN_PROGRESSION': setChordInProgression,
    'DELETE_CHORD_FROM_PROGRESSION': deleteChordFromProgression,
    'SET_NEW_SETTINGS': setNewSettings,
    'START_PLAYING_PROGRESSION': startPlayingProgression,
    'SET_CHORD_PLAYING_STATE': setChordPlayingState,
    'STOP_PLAYING': stopPlaying
  }
);

function loadProgressionDetailSuccess(state, action){
  // at this point, barsChordsIds is filled with newly received chords ids
  // from the API, grouped by bars
  // we need to load the corresponding chord objects from staticData
  // we use the map of chords (keys are their ids) for that

  let newProgressionContext = Object.assign({}, action.actionContext.newProgressionContext);
  newProgressionContext._bars = [];
  for(var i = 0; i < newProgressionContext.barsChordsIds.length; i++){
    newProgressionContext._bars.push([]);
    for (var j = 0; j < newProgressionContext.barsChordsIds[i].length; j++) {
      let correspondingChord = Object.assign(
        {},
        action.actionContext.staticData.chordsMap.get(newProgressionContext.barsChordsIds[i][j]),
        {_localization: {barIndex: i, chordIndex: j}}
      );
      newProgressionContext._bars[i].push(correspondingChord);
    }
  }
  let newState = Object.assign({}, state, {
    id: newProgressionContext.id,
    title: newProgressionContext.title,
    description: newProgressionContext.description,
    barsChordsIds: newProgressionContext.barsChordsIds,
    _bars: newProgressionContext._bars
  });
  return newState;
}

function clearCurrentProgression(state, action) {
  var emptyProgression = Object.assign({}, initialState.progressionContext);
  return emptyProgression;
}

function setNewSettings(state, action){
  return Object.assign({}, state, {
    title: action.newSettings.newTitle,
    description: action.newSettings.newDescription
  });
}

function deleteChordFromProgression(state, action){
  let chordsAfterTheOneDeleted = [...state.chords.slice(action.position + 1)];
  for(let i = 0; i < chordsAfterTheOneDeleted.length; i++){
    chordsAfterTheOneDeleted[i] = updateObject(chordsAfterTheOneDeleted[i], {positionInSequence: chordsAfterTheOneDeleted[i].positionInSequence - 1});
  }
  let newChords = [...state.chords.slice(0, action.position), ...chordsAfterTheOneDeleted];
  return Object.assign({}, state, { chords: newChords });
}

function setChordInProgression(state, action) {
  let newChord = Object.assign({}, state.editedChord, { isPlaying:false });
  let newBars = deepCloneArray(state._bars);

  if(newChord._localization.barIndex === newBars.length) {
    newBars.push([newChord]);
  } else {
    // other chords have to be modified according to localization
    if(newBars[newChord._localization.barIndex].length === 1) {
      let previousChord = newBars[newChord._localization.barIndex][0];
      let reducedPreviousChord = findChordFromChange(previousChord,  )
    }
    newBars[newChord._localization.barIndex][newChord._localization.chordIndex] = newChord;
  }

  return updateObject(state, {_bars: newBars, editedChord:{}});
}

function startPlayingProgression(state, action){
  return state;
}

function setChordPlayingState(state, {actionContext}) {
  let newBars = deepCloneArray(state._bars);
  newBars[actionContext.barIndex][actionContext.chordIndex].isPlaying = actionContext.newPlayingState;
  return updateObject(state, {_bars: newBars});
}

function stopPlaying(state, action) {
  let newProgressionBars = [];
  for(var i = 0; i < state._bars.length; i++){
    newProgressionBars.push([]);
    for (var j = 0; j < state._bars[i].length; j++) {
      newProgressionBars[i][j] = updateObject(state._bars[i][j], {hasBeenStarted: false, isPlaying: false });
    }
  }
  return updateObject(state, {_bars: newProgressionBars});
}
function emptyProgressionDetails(state, action){
  let newProgressionContext = {};
  return updateObject(state, { progressionContext: newProgressionContext });
}
