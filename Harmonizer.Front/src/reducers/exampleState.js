
export default {
  progressionsList: [],
  progressionContext: {
    id: 354,
    title: "Get up, stand up !",
    description: "A Bob Marley's classic",
    _bars: [
      [
        {
          enharmonicNoteId : "a",
          id : 3,
          isPlaying : false,
          notation : "A7",
          rootNoteId : "a",
          typeId : "dom7",
          valueId : 2
        },
      ]
    ],
    barsChordsIds: []
  },
  editedChord: {
    enharmonicNoteId : "a",
    id : 3,
    isPlaying : false,
    notation : "A7",
    rootNoteId : "a",
    typeId : "dom7",
    valueId : 2
  },
  staticData: {},
  appState: {
    editMode: false,
    isEditingTitle: false,
    isEditingSettings: false,
    isPlayingProgression: false,
    isProgressionDirty: false,
    numAjaxCallsInProgress: 0
  },
  playerData: {
    playMode: 'progression', // could also be 'chordInProgression', 'chordOutOfContext' or 'bar'
    isPlaying: false,
    playingIndex: 0,
    segments: [],
    metronome: false
  },
  delayedProgressionChange: {
    action: null, // 'split', 'add'
    chordIndex: null, // int
    barIndex: null, // int
    direction: null // 'left', 'right'
  }
}
