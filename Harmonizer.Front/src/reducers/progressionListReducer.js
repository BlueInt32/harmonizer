import initialState from './initialState';
import { createReducer } from './utils';

function loadProgressions(state, action){
  return action.progressions;
}

export default createReducer(
  initialState.progressionsList, 
  {
    'LOAD_PROGRESSIONS_SUCCESS': loadProgressions
  }
);
