export default {
  progressionsList: [],
  progressionContext: {
    id: 0,
    title: "",
    description: "",
    _bars: [],
    barsChordsIds: []
  },
  editedChord: {
  },
  staticData: {},
  appState: {
    editMode: false,
    isEditingTitle: false,
    isEditingSettings: false,
    isPlayingProgression: false,
    isProgressionDirty: false,
    numAjaxCallsInProgress: 0
  },
  playerData: {
    playMode: 'progression', // could also be 'chordInProgression', 'chordOutOfContext' or 'bar'
    isPlaying: false,
    playingIndex: 0,
    segments: [],
    metronome: false
  },
  delayedProgressionChange: {
    action: null, // 'split', 'add'
    chordIndex: null, // int
    barIndex: null, // int
    direction: null // 'left', 'right'
  }
}
