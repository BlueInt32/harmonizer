import initialState from './initialState';
import {updateObject} from '../common';

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8) === '_SUCCESS';
}

export default function appStateReducer(state = initialState.appState, action) {
  if (action.type === 'BEGIN_AJAX_CALL') {
    return updateObject(state, { numAjaxCallsInProgress: state.numAjaxCallsInProgress + 1 });
  } else if (action.type === 'AJAX_CALL_ERROR' || actionTypeEndsInSuccess(action.type)) {
    return updateObject(state, { numAjaxCallsInProgress: state.numAjaxCallsInProgress - 1 });
  } else if(action.type === 'SET_STATE_IS_EDITING_CHORD'){
    return updateObject(state, { isEditingChord: action.newState });
  } else if(action.type === 'CANCEL_CHORD_EDITION'){
    return updateObject(state, { isEditingChord: action.newState });
  } else if (action.type === 'SET_STATE_IS_EDITING_SETTINGS'){
    return updateObject(state, {isEditingSettings: action.newState});
  } else if (action.type === 'TOGGLE_EDIT_MODE'){
    return updateObject(state, {editMode: !state.editMode});
  }
  return state;
}
