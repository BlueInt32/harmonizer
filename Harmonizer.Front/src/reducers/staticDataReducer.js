import initialState from './initialState';

export default function staticDataReducer(state = initialState.staticData, action) {
  switch (action.type) {
    case 'LOAD_STATIC_DATA_SUCCESS':
      let chordsMap = new Map();
      action.staticData.chords.forEach((chord) => {chordsMap.set(chord.id, chord)});
      action.staticData.chordsMap = chordsMap;

      return action.staticData;
    case 'GET_CHORD_NOTATION':
      let chordInfo = state.chords.filter(c => c.id === action.chordId)[0];
      let noteInfo = state.notes.filter(n => n.id === chordInfo.rootNoteId)[0];
      let chordTypeInfo = state.chordTypes.filter(t => t.id === chordInfo.typeId)[0];

      return `${noteInfo.name}${chordTypeInfo.notation}`;
    case 'FIND_CHORDID_BY_DETAILS_IDS':
      let correspondingStaticChord = state.chords.filter(c =>
          c.rootNoteId === action.chordSetOfIds.rootNoteId
          && c.typeId === action.chordSetOfIds.typeId
          && c.valueId === action.chordSetOfIds.valueId
      )[0];
      return correspondingStaticChord;
    default:
      return state;
  }
}
