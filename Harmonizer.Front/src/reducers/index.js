import { combineReducers } from 'redux';
import progressionsList from './progressionListReducer';
import staticData from './staticDataReducer';
import progressionContext from './progressionContextReducer';
import editedChord from './editedChordReducer';
import appState from './appStateReducer';
import playerData from './playerDataReducer';
import { routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({
  progressionContext,
  editedChord,
  progressionsList,
  staticData,
  appState,
  playerData,
  router: routerReducer,
});

export default rootReducer;
