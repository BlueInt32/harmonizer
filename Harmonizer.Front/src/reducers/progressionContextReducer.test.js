import expect from 'expect';
import progressionContextReducer from './progressionContextReducer';
import * as apiActions from '../actions/apiActions';
import * as progressionContextActions from '../actions/progressionContextActions';
import * as mock  from '../api/mockData';

describe('Progression reducer', () => {
  it('should update progression details when passed LOAD_PROGRESSION_DETAILS_SUCCESS', () => {
    // arrange
    const initialState = {
      id: 0,
      title: "",
      description: "",
      chords: [],
      editedChord: {
        commitPosition: 0,
        rootNoteId: "",
        typeId: "maj",
        valueId: 2
      }
    };

    const action = apiActions.loadProgressionDetailsSuccess(mock.progressionDetails);

    // act
    const newState = progressionContextReducer(initialState, action);

    // assert
    expect(newState.title).toEqual(mock.progressionDetails.title);
  });
  it('should clean up progression Context when passed CLEAR_CURRENT_PROGRESSION', () => {
    // arrange
    const initialState = {
      id: 1,
      title: "foo",
      description: "bar",
      chords: [
        {
          "id": 866,
          "positionInSequence": 0,
          "chordId": 1,
          "chord": {
            "id": 1,
            "rootNoteId": "a",
            "rootNote": {
              "id": "a ",
              "name": "A"
            },
            "typeId": "dom7",
            "chordType": {
              "id": "dom7",
              "name": "Dominant Seventh",
              "notation": "7",
              "description": "",
              "spriteOffset": 7200
            },
            "valueId": 1,
            "value": {
              "id": 1,
              "name": "Quarter Note (1)"
            },
            "notation": "A7"
          }
        },
        {
          "id": 867,
          "positionInSequence": 1,
          "chordId": 4,
          "chord": {
            "id": 4,
            "rootNoteId": "ab",
            "rootNote": {
              "id": "ab",
              "name": "A♭"
            },
            "typeId": "dom7",
            "chordType": {
              "id": "dom7",
              "name": "Dominant Seventh",
              "notation": "7",
              "description": "",
              "spriteOffset": 7200
            },
            "valueId": 1,
            "value": {
              "id": 1,
              "name": "Quarter Note (1)"
            },
            "notation": "A♭7"
          }
        }
      ],
      editedChord: {
        commitPosition: 4,
        noteId: "a",
        typeId: "dom7",
        valueId: 4
      }
    };

    const action = progressionContextActions.clearCurrentProgression();

    // act
    const newState = progressionContextReducer(initialState, action);

    // assert
    expect(newState.id).toEqual(0);
    expect(newState.title).toEqual('');
    expect(newState.description).toEqual('');
    expect(newState.chords.length).toEqual(0);
  });
});
