import initialState from './initialState';

export default function staticDataReducer(state = initialState.playerData, action) {
  switch (action.type) {
    case 'TOGGLE_METRONOME':
      return Object.assign({}, state, {
        metronome: !state.metronome
      });

    case 'START_PLAYING_CHORD_IN_PROGRESSION':
      return Object.assign({}, state, {
        isPlaying: true,
        playerSegments: action.context.playerSegments,
        localization: action.context.chord._localization,
        playMode: 'chordInProgression'
      });
    case 'START_PLAYING_CHORD_OUT_OF_CONTEXT':
      return Object.assign({}, state, {
        isPlaying: true,
        playerSegments: [[action.context.chord]],
        playMode: 'chordOutOfContext'
      });

    case 'START_PLAYING_PROGRESSION':
      return Object.assign({}, state, {
        isPlaying: true,
        playerSegments: action.playerSegments,
        playMode: 'progression'
      });
    case 'STOP_PLAYING':
      return Object.assign({}, state, {
        isPlaying: false
      });
    default:
      return state;
  }
}
