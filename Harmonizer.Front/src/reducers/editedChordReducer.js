import initialState from './initialState';
import { createReducer } from './utils';

export default createReducer(
  initialState.editedChord,
  {
    'SET_EDITED_CHORD': setEditedChord
  }
);

function setEditedChord(state, action){
  return action.newEditedChord;
}
