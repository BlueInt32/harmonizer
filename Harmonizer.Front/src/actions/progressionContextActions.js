import { findChordFromChange, deepCloneArray } from '../common';

export function clearCurrentProgression() {
  return {
    type: 'CLEAR_CURRENT_PROGRESSION'
  };
}

export function replaceBarInProgression(){
  return (dispatch, getState) => {
    const state = getState();
    const editedChord = state.editedChord;
    //
    let editedBar = deepCloneArray(state.progressionContext._bars[editedChord._localization.barIndex]);
    if(editedBar.length === 1) {
      let transformedEditedChord = Object.assign(
        {},
        editedChord,
        findChordFromChange(editedChord, state.staticData.chords, { valueId: 2 }),
        { _localization: { barIndex: editedChord._localization.barIndex, chordIndex: 0 } }
      );
      if(editedChord._localization.addChordInBar === 'left'){
        let otherChord = Object.assign(
          {},
          editedBar[0],
          findChordFromChange(editedBar[0], state.staticData.chord, { valueId: 2 }),
          { _localization: { barIndex: editedChord._localization.barIndex, chordIndex: 1 } }
        );

        editedBar = [transformedEditedChord, otherChord];
      }
      else {

      }
    } else if(editedBar.length === 2) {

    }

    dispatch({ type: 'REPLACE_BAR_IN_PROGRESSION' });
  };
}

export function setChordPlayingState(barIndex, chordIndex, newPlayingState){
  return {
    type: 'SET_CHORD_PLAYING_STATE',
    actionContext: {barIndex, chordIndex, newPlayingState}
  };
}

export function deleteChordFromProgression(position) {
  return {
    type: 'DELETE_CHORD_FROM_PROGRESSION',
    position: position
  };
}
