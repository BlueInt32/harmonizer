import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';
import progressionApi from '../api/progressionApi';

export function saveProgression() {
  return function(dispatch, getState) {
    dispatch(beginAjaxCall("Save Progression Details"));
    let state = getState();
    let progression = {
      id: state.progressionContext.id,
      title: state.progressionContext.title,
      description: state.progressionContext.description,
      tempo: 70,
      chords: state.progressionContext.chords.map(c => c.id)
    };
    return progressionApi.saveProgression(progression).then(savedProgression => {
      progression.id
        ? dispatch(updateProgressionSuccess(savedProgression))
        : dispatch(saveProgressionSuccess(savedProgression));
    })
      .catch(error => {
        dispatch(ajaxCallError(error));
        throw (error);
      });
  }
}

export function saveProgressionSuccess(progression) {
  return {
    type: 'SAVE_PROGRESSIONS_SUCCESS',
    progression
  };
}

export function loadProgressionDetails(progressionId) {
  return function(dispatch, getState) {
    dispatch(beginAjaxCall("Load Progression Details"));
    const state = getState();
    dispatch(emptyProgressionDetails())
    return progressionApi.getProgressionDetails(progressionId).then(newProgressionContext => {
      dispatch(loadProgressionDetailsSuccess(newProgressionContext, state.staticData));
    })
    .catch(error => {
      throw (error);
    });
  }
}

export function emptyProgressionDetails() {
  return {
    type: 'EMPTY_PROGRESSION_DETAILS'
  };
}

export function loadProgressionDetailsSuccess(newProgressionContext, staticData) {
  return {
    type: 'LOAD_PROGRESSION_DETAILS_SUCCESS',
    actionContext: {newProgressionContext, staticData}
  };
}

export function updateProgressionSuccess(progression) {
  return {
    type: 'UPDATE_PROGRESSIONS_SUCCESS',
    progression
  };
}

export function loadProgressionsSuccess(progressions) {
  return {
    type: 'LOAD_PROGRESSIONS_SUCCESS',
    progressions
  };
}

export function loadProgressions() {
  return function(dispatch) {
    dispatch(beginAjaxCall("Load progressions list"));
    return progressionApi.getAllProgressions().then(progressions => {
      dispatch(loadProgressionsSuccess(progressions));
    })
      .catch(error => {
        throw (error);
      });
  }
}
