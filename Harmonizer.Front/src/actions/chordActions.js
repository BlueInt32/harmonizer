import { findChordFromChange } from '../common';
import * as playerActions from './playerActions';
import * as routeActions from 'react-router-redux';
import { once } from 'redux-when';

// Composite actions
export function startChordEditionComposite(progressionId, chordLocalization) {
  return (dispatch, getState) => {
    const state = getState();
    let editedChord = null;
    if(chordLocalization.addBar || chordLocalization.addChordInBar){
      editedChord = getDefaultChord(state.staticData);
    } else {
      editedChord = {...state.progressionContext._bars[chordLocalization.barIndex][chordLocalization.chordIndex]};
    }
    Object.assign(editedChord, { _localization: chordLocalization });
    dispatch(playerActions.startPlayingChordOutOfContext(editedChord));
    
    dispatch(once(state => state.editedChord.id, () => {
      console.log("state editedChord ready");
      return routeActions.push(`/progression/${progressionId}/chord`);
      //dispatch();
    }));

    dispatch(replaceEditedChord(editedChord));

  }
}

export function changeEditedChordComposite(modifier) {
  return (dispatch, getState) => {
    const state  = getState();
    let newChord = findChordFromChange(
      state.editedChord,
      state.staticData.chords,
      modifier
    );

    dispatch(playerActions.startPlayingChordOutOfContext(newChord));
    dispatch(replaceEditedChord(newChord));
  }
}

export function replaceEditedChord(newEditedChord){
    return {
      type: 'SET_EDITED_CHORD',
      newEditedChord
    };
}

function getDefaultChord(staticData) {
  let defaultChord = {
      valueId: staticData.defaultValueId,
      rootNoteId : staticData.defaultNoteId,
      enharmonicNoteId: staticData.defaultEnharmonicNoteId,
      typeId : staticData.defaultTypeId,
    };
  return defaultChord;
}
