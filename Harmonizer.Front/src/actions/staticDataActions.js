import staticDataApi from '../api/mockStaticDataApi';
import {beginAjaxCall} from './ajaxStatusActions';

export function loadStaticDataSuccess(staticData) {
  return {
    type: 'LOAD_STATIC_DATA_SUCCESS',
    staticData
  };
}
export function findChordIdByDetailIds(chordSetOfIds) {
  return {
    type: 'FIND_CHORDID_BY_DETAILS_IDS',
    chordSetOfIds
  };
}
export function loadStaticData() {
  return function (dispatch) {
    dispatch(beginAjaxCall("Load Static Data"));
    return staticDataApi.getAllStaticData().then(staticData => {
      dispatch(loadStaticDataSuccess(staticData));
    })
      .catch(error => {
        throw(error);
      });
  }
}
