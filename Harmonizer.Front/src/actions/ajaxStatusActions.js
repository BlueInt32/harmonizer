export function beginAjaxCall(info) {
  return {
    type: 'BEGIN_AJAX_CALL',
    info
  };
}
export function ajaxCallError() {
  return {type: 'AJAX_CALL_ERROR'};
}
