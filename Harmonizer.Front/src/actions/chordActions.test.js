import expect from 'expect';
import * as chordActions from './apiActions';

import thunk from 'redux-thunk';
import nock from 'nock';
import configureMockStore from 'redux-mock-store';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

// Test a sync action
describe('Chord actions', () => {
});


describe('Async Actions', () => {
  afterEach(() => {
    nock.cleanAll();
  });
});
