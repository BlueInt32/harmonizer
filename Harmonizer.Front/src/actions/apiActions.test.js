import expect from 'expect';
import * as apiActions from './apiActions';

import thunk from 'redux-thunk';
import nock from 'nock';
import configureMockStore from 'redux-mock-store';

// Test a sync action
describe('ProgressionContext actions', () => {
  describe('saveProgressionSuccess', () => {
    it('should create a SAVE_PROGRESSIONS_SUCCESS action', () => {
      //arrange
      const progression = {
        id: 'clean-code',
        title: 'Clean Code'
      };
      const expectedAction = {
        type: 'SAVE_PROGRESSIONS_SUCCESS',
        progression: progression
      };

      //act
      const action = apiActions.saveProgressionSuccess(progression);

      //assert
      expect(action).toEqual(expectedAction);
    });
  });

});

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Async Actions', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('should create BEGIN_AJAX_CALL and LOAD_PROGRESSIONS_SUCCESS when loading progressions', (done) => {
    // Here's an example call to nock.
    // nock('http://example.com/')
    //   .get('/courses')
    //   .reply(200, { body: { course: [{ id: 1, firstName: 'Cory', lastName: 'House'}] }});

    const expectedActions = [
      {
        type: 'BEGIN_AJAX_CALL'
      },
      {
        type: 'LOAD_PROGRESSIONS_SUCCESS',
        body: {
          progressions: [{
            id: 'clean-code',
            title: 'Clean Code'
          }]
        }
      }
    ];

    const store = mockStore({
      progressions: []
    }, expectedActions, done);
    store.dispatch(apiActions.loadProgressions()).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual('BEGIN_AJAX_CALL');
      expect(actions[1].type).toEqual('LOAD_PROGRESSIONS_SUCCESS');
      done();
    });
  });

  it('should create BEGIN_AJAX_CALL and LOAD_PROGRESSION_DETAILS_SUCCESS when loading progression details', (done) => {
    // Here's an example call to nock.
    // nock('http://example.com/')
    //   .get('/courses')
    //   .reply(200, { body: { course: [{ id: 1, firstName: 'Cory', lastName: 'House'}] }});

    const expectedActions = [
      {
        type: 'BEGIN_AJAX_CALL'
      },
      {
        type: 'LOAD_PROGRESSION_DETAILS_SUCCESS',
        body: {
          progressions: [{
            id: 'clean-code',
            title: 'Clean Code'
          }]
        }
      }
    ];

    const store = mockStore({
      progressions: []
    }, expectedActions, done);
    store.dispatch(apiActions.loadProgressionDetails()).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual('BEGIN_AJAX_CALL');
      expect(actions[1].type).toEqual('LOAD_PROGRESSION_DETAILS_SUCCESS');
      done();
    });
  });

  it('should create BEGIN_AJAX_CALL and SAVE_PROGRESSIONS_SUCCESS when saving a new progression (no id provided)', (done) => {
    // Here's an example call to nock.
    // nock('http://example.com/')
    //   .get('/courses')
    //   .reply(200, { body: { course: [{ id: 1, firstName: 'Cory', lastName: 'House'}] }});

    const expectedActions = [
      {
        type: 'BEGIN_AJAX_CALL'
      },
      {
        type: 'SAVE_PROGRESSIONS_SUCCESS',
        body: {
          progressions: [{
            id: 'clean-code',
            title: 'Clean Code'
          }]
        }
      }
    ];
    const progressionToSave = {
      title: "hello" 
    };

    const store = mockStore({
      progressions: []
    }, expectedActions, done);
    store.dispatch(apiActions.saveProgression(progressionToSave)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual('BEGIN_AJAX_CALL');
      expect(actions[1].type).toEqual('SAVE_PROGRESSIONS_SUCCESS');
      done();
    });
  });

  it('should create BEGIN_AJAX_CALL and UPDATE_PROGRESSIONS_SUCCESS when saving an existing progression (id provided)', (done) => {
    // Here's an example call to nock.
    // nock('http://example.com/')
    //   .get('/courses')
    //   .reply(200, { body: { course: [{ id: 1, firstName: 'Cory', lastName: 'House'}] }});

    const expectedActions = [
      {
        type: 'BEGIN_AJAX_CALL'
      },
      {
        type: 'UPDATE_PROGRESSIONS_SUCCESS',
        body: {
          progressions: [{
            id: 'clean-code',
            title: 'Clean Code'
          }]
        }
      }
    ];
    const progressionToSave = {
      id: 435,
      title: "hello" 
    };

    const store = mockStore({
      progressions: []
    }, expectedActions, done);
    store.dispatch(apiActions.saveProgression(progressionToSave)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual('BEGIN_AJAX_CALL');
      expect(actions[1].type).toEqual('UPDATE_PROGRESSIONS_SUCCESS');
      done();
    });
  });
});
