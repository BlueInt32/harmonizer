
export function startSettingsEdition(settings){
  return [
    setStateToIsEditingSettings(true)
  ];
}

export function toggleEditMode(isOn) {
  return {
    type: 'TOGGLE_EDIT_MODE',
    isOn
  };
}

export function cancelSettingsEdition(settings){
  return [
    setStateToIsEditingSettings(false)
  ];
}

export function commitSettingsEdition(settings){
  return [
    setStateToIsEditingSettings(false),
    setNewSettings(settings)
  ];
}

function setStateToIsEditingSettings(newState){
  return {
    type: 'SET_STATE_IS_EDITING_SETTINGS',
    newState
  };
}

function setNewSettings(newSettings){
  return {
    type: 'SET_NEW_SETTINGS',
    newSettings
  };
}
