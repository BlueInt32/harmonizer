
export function playSound() {
  return {
    type: 'PLAY_SOUND'
  };
}
export function stopPlaying() {
  return {
    type: 'STOP_PLAYING'
  };
}

export function startPlayingChordInProgression(chord){
  return (dispatch, getState) => {
    const state = getState();
    var playerSegments = state.progressionContext._bars;
    dispatch({
      type: 'START_PLAYING_CHORD_IN_PROGRESSION',
      context: { playerSegments, chord }
    });
  }
}
export function startPlayingChordOutOfContext(chord){
  return {
    type: 'START_PLAYING_CHORD_OUT_OF_CONTEXT',
    context: { chord }
  };   
}

export function toggleMetronome() {
  return {
    type: 'TOGGLE_METRONOME'
  }
}
export function startPlayingProgression(){
  return (dispatch, getState) => {
    const state = getState();
    var playerSegments = state.progressionContext._bars;
    dispatch({
      type: 'START_PLAYING_PROGRESSION',
      playerSegments
    });
  }
}
