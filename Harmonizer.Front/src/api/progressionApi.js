import settings from '../settings';
import MockProgressionApi from './mockProgressionApi';


const apiHostUrl = ""; // proxied in dev, see proxy in package.json
function get(url){
  return new Promise((resolve, reject) => {
    fetch(url).then(response => {
      if(response.ok){
        response.json().then(json => {
          resolve(json);
        });
      }
    }).catch(function(error) {
      console.log('%cFetch fail calling ' + url + ': ' + error.message, "color: white; background-color: red;font-style: italic; padding: 2px;margin:4px");
    });
  });
}

function post(url, body) {
  return new Promise((resolve, reject) => {
    let callSettings = {
      method: 'POST',
      body
    };
    fetch(url, callSettings).then(response => {
      if(response.ok) {
        response.json().then(json => {
          resolve(json);
        })
      }
    }).catch((error) => {
      console.log('%cFetch fail posting on ' + url + ': ' + error.message, "color: white; background-color: red;font-style: italic; padding: 2px;margin:4px")
    })
  })
}
function put(url, body) {
  return new Promise((resolve, reject) => {
    let headers = new Headers({ "Accept": "application/json" });
    headers = {"Content-type": "application/json","Accept": "application/json"};
    delete body.id;
    body = JSON.stringify(body);
    let callSettings = {
      method: 'PUT',
      body,
      headers
    };
    fetch(url, callSettings)
      .then(response => {if(response.ok) {resolve();} } )
      .catch((error) => {
        console.log('%cFetch fail posting on ' + url + ': ' + error.message, "color: white; background-color: red;font-style: italic; padding: 2px;margin:4px")
      })
  })
}
class ProgressionApi {
  static getAllProgressions() {
    if(settings.useMock){
      return MockProgressionApi.getAllProgressions();
    }
    return get(apiHostUrl + "/api/progressions");
  }
  static getProgressionDetails(id) {
    if(settings.useMock){
      return MockProgressionApi.getProgressionDetails(id);
    }
    return get(apiHostUrl + "/api/progressions/"+id);
  }
  static saveProgression(progression) {
    if(settings.useMock){
      return MockProgressionApi.saveProgression(progression);
    }
    if(progression.id) {
      return put(apiHostUrl + "/api/progressions/" + progression.id, progression);
    }
    else {
      return post(apiHostUrl + "/api/progressions", progression);
    }
  }
}

export default ProgressionApi
