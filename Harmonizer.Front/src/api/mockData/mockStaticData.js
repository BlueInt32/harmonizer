const staticData = {
    "tempos": [
        {
            "id": 70,
            "name": "slow (70bpm)"
        },
        {
            "id": 85,
            "name": "85 bpm"
        },
        {
            "id": 100,
            "name": "100 bpm"
        },
        {
            "id": 115,
            "name": "115 bpm"
        },
        {
            "id": 130,
            "name": "fast (130bpm)"
        }
    ],
    "defaultTempoId": 100,
    "notes": [
        {
            "id": "a",
            "name": "A",
            "enharmonicNoteId": null
        },
        {
            "id": "as",
            "name": "A♯",
            "enharmonicNoteId": "bf"
        },
        {
            "id": "bf",
            "name": "B♭",
            "enharmonicNoteId": null
        },
        {
            "id": "b",
            "name": "B",
            "enharmonicNoteId": null
        },
        {
            "id": "c",
            "name": "C",
            "enharmonicNoteId": null
        },
        {
            "id": "cs",
            "name": "C♯",
            "enharmonicNoteId": "df"
        },
        {
            "id": "df",
            "name": "D♭",
            "enharmonicNoteId": null
        },
        {
            "id": "d",
            "name": "D",
            "enharmonicNoteId": null
        },
        {
            "id": "ds",
            "name": "D♯",
            "enharmonicNoteId": "ef"
        },
        {
            "id": "ef",
            "name": "E♭",
            "enharmonicNoteId": null
        },
        {
            "id": "e",
            "name": "E",
            "enharmonicNoteId": null
        },
        {
            "id": "f",
            "name": "F",
            "enharmonicNoteId": null
        },
        {
            "id": "fs",
            "name": "F♯",
            "enharmonicNoteId": "gf"
        },
        {
            "id": "gf",
            "name": "G♭",
            "enharmonicNoteId": null
        },
        {
            "id": "g",
            "name": "G",
            "enharmonicNoteId": null
        },
        {
            "id": "gs",
            "name": "G♯",
            "enharmonicNoteId": "af"
        },
        {
            "id": "af",
            "name": "A♭",
            "enharmonicNoteId": null
        }
    ],
    "defaultNoteId": "c",
    "defaultEnharmonicNoteId": "c",
    "chordTypes": [
        {
            "id": "maj",
            "name": "Major Triad",
            "notation": "",
            "description": "",
            "spriteOffset": 0
        },
        {
            "id": "min",
            "name": "Minor Triad",
            "notation": "m",
            "description": "",
            "spriteOffset": 3600
        },
        {
            "id": "dom7",
            "name": "Dominant Seventh",
            "notation": "7",
            "description": "",
            "spriteOffset": 7200
        },
        {
            "id": "maj7",
            "name": "Major Seventh",
            "notation": "M7",
            "description": "",
            "spriteOffset": 10800
        }
    ],
    "defaultTypeId": "maj",
    "values": [
        {
            "id": 1,
            "name": "Quarter Note (1)"
        },
        {
            "id": 2,
            "name": "Half Note (2)"
        },
        {
            "id": 4,
            "name": "Whole Note (4)"
        }
    ],
    "defaultValueId": 2,
    "chords": [
        {
            "id": 1,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "A7"
        },
        {
            "id": 2,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "A7"
        },
        {
            "id": 3,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "A7"
        },
        {
            "id": 4,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "A♭7"
        },
        {
            "id": 5,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "A♭7"
        },
        {
            "id": 6,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "A♭7"
        },
        {
            "id": 7,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "A♯7"
        },
        {
            "id": 8,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "A♯7"
        },
        {
            "id": 9,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "A♯7"
        },
        {
            "id": 10,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "B7"
        },
        {
            "id": 11,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "B7"
        },
        {
            "id": 12,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "B7"
        },
        {
            "id": 13,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "B♭7"
        },
        {
            "id": 14,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "B♭7"
        },
        {
            "id": 15,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "B♭7"
        },
        {
            "id": 16,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "C7"
        },
        {
            "id": 17,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "C7"
        },
        {
            "id": 18,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "C7"
        },
        {
            "id": 19,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "C♯7"
        },
        {
            "id": 20,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "C♯7"
        },
        {
            "id": 21,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "C♯7"
        },
        {
            "id": 22,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "D7"
        },
        {
            "id": 23,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "D7"
        },
        {
            "id": 24,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "D7"
        },
        {
            "id": 25,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "D♭7"
        },
        {
            "id": 26,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "D♭7"
        },
        {
            "id": 27,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "D♭7"
        },
        {
            "id": 28,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "D♯7"
        },
        {
            "id": 29,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "D♯7"
        },
        {
            "id": 30,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "D♯7"
        },
        {
            "id": 31,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "E7"
        },
        {
            "id": 32,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "E7"
        },
        {
            "id": 33,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "E7"
        },
        {
            "id": 34,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "E♭7"
        },
        {
            "id": 35,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "E♭7"
        },
        {
            "id": 36,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "E♭7"
        },
        {
            "id": 37,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "F7"
        },
        {
            "id": 38,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "F7"
        },
        {
            "id": 39,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "F7"
        },
        {
            "id": 40,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "F♯7"
        },
        {
            "id": 41,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "F♯7"
        },
        {
            "id": 42,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "F♯7"
        },
        {
            "id": 43,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "G7"
        },
        {
            "id": 44,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "G7"
        },
        {
            "id": 45,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "G7"
        },
        {
            "id": 46,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "G♭7"
        },
        {
            "id": 47,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "G♭7"
        },
        {
            "id": 48,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "G♭7"
        },
        {
            "id": 49,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "dom7",
            "valueId": 1,
            "notation": "G♯7"
        },
        {
            "id": 50,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "dom7",
            "valueId": 2,
            "notation": "G♯7"
        },
        {
            "id": 51,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "dom7",
            "valueId": 4,
            "notation": "G♯7"
        },
        {
            "id": 52,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "maj",
            "valueId": 1,
            "notation": "A"
        },
        {
            "id": 53,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "maj",
            "valueId": 2,
            "notation": "A"
        },
        {
            "id": 54,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "maj",
            "valueId": 4,
            "notation": "A"
        },
        {
            "id": 55,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "maj",
            "valueId": 1,
            "notation": "A♭"
        },
        {
            "id": 56,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "maj",
            "valueId": 2,
            "notation": "A♭"
        },
        {
            "id": 57,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "maj",
            "valueId": 4,
            "notation": "A♭"
        },
        {
            "id": 58,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "maj",
            "valueId": 1,
            "notation": "A♯"
        },
        {
            "id": 59,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "maj",
            "valueId": 2,
            "notation": "A♯"
        },
        {
            "id": 60,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "maj",
            "valueId": 4,
            "notation": "A♯"
        },
        {
            "id": 61,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "maj",
            "valueId": 1,
            "notation": "B"
        },
        {
            "id": 62,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "maj",
            "valueId": 2,
            "notation": "B"
        },
        {
            "id": 63,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "maj",
            "valueId": 4,
            "notation": "B"
        },
        {
            "id": 64,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "maj",
            "valueId": 1,
            "notation": "B♭"
        },
        {
            "id": 65,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "maj",
            "valueId": 2,
            "notation": "B♭"
        },
        {
            "id": 66,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "maj",
            "valueId": 4,
            "notation": "B♭"
        },
        {
            "id": 67,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "maj",
            "valueId": 1,
            "notation": "C"
        },
        {
            "id": 68,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "maj",
            "valueId": 2,
            "notation": "C"
        },
        {
            "id": 69,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "maj",
            "valueId": 4,
            "notation": "C"
        },
        {
            "id": 70,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "maj",
            "valueId": 1,
            "notation": "C♯"
        },
        {
            "id": 71,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "maj",
            "valueId": 2,
            "notation": "C♯"
        },
        {
            "id": 72,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "maj",
            "valueId": 4,
            "notation": "C♯"
        },
        {
            "id": 73,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "maj",
            "valueId": 1,
            "notation": "D"
        },
        {
            "id": 74,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "maj",
            "valueId": 2,
            "notation": "D"
        },
        {
            "id": 75,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "maj",
            "valueId": 4,
            "notation": "D"
        },
        {
            "id": 76,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "maj",
            "valueId": 1,
            "notation": "D♭"
        },
        {
            "id": 77,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "maj",
            "valueId": 2,
            "notation": "D♭"
        },
        {
            "id": 78,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "maj",
            "valueId": 4,
            "notation": "D♭"
        },
        {
            "id": 79,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "maj",
            "valueId": 1,
            "notation": "D♯"
        },
        {
            "id": 80,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "maj",
            "valueId": 2,
            "notation": "D♯"
        },
        {
            "id": 81,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "maj",
            "valueId": 4,
            "notation": "D♯"
        },
        {
            "id": 82,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "maj",
            "valueId": 1,
            "notation": "E"
        },
        {
            "id": 83,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "maj",
            "valueId": 2,
            "notation": "E"
        },
        {
            "id": 84,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "maj",
            "valueId": 4,
            "notation": "E"
        },
        {
            "id": 85,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "maj",
            "valueId": 1,
            "notation": "E♭"
        },
        {
            "id": 86,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "maj",
            "valueId": 2,
            "notation": "E♭"
        },
        {
            "id": 87,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "maj",
            "valueId": 4,
            "notation": "E♭"
        },
        {
            "id": 88,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "maj",
            "valueId": 1,
            "notation": "F"
        },
        {
            "id": 89,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "maj",
            "valueId": 2,
            "notation": "F"
        },
        {
            "id": 90,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "maj",
            "valueId": 4,
            "notation": "F"
        },
        {
            "id": 91,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "maj",
            "valueId": 1,
            "notation": "F♯"
        },
        {
            "id": 92,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "maj",
            "valueId": 2,
            "notation": "F♯"
        },
        {
            "id": 93,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "maj",
            "valueId": 4,
            "notation": "F♯"
        },
        {
            "id": 94,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "maj",
            "valueId": 1,
            "notation": "G"
        },
        {
            "id": 95,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "maj",
            "valueId": 2,
            "notation": "G"
        },
        {
            "id": 96,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "maj",
            "valueId": 4,
            "notation": "G"
        },
        {
            "id": 97,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "maj",
            "valueId": 1,
            "notation": "G♭"
        },
        {
            "id": 98,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "maj",
            "valueId": 2,
            "notation": "G♭"
        },
        {
            "id": 99,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "maj",
            "valueId": 4,
            "notation": "G♭"
        },
        {
            "id": 100,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "maj",
            "valueId": 1,
            "notation": "G♯"
        },
        {
            "id": 101,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "maj",
            "valueId": 2,
            "notation": "G♯"
        },
        {
            "id": 102,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "maj",
            "valueId": 4,
            "notation": "G♯"
        },
        {
            "id": 103,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "AM7"
        },
        {
            "id": 104,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "AM7"
        },
        {
            "id": 105,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "AM7"
        },
        {
            "id": 106,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "A♭M7"
        },
        {
            "id": 107,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "A♭M7"
        },
        {
            "id": 108,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "A♭M7"
        },
        {
            "id": 109,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "A♯M7"
        },
        {
            "id": 110,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "A♯M7"
        },
        {
            "id": 111,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "A♯M7"
        },
        {
            "id": 112,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "BM7"
        },
        {
            "id": 113,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "BM7"
        },
        {
            "id": 114,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "BM7"
        },
        {
            "id": 115,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "B♭M7"
        },
        {
            "id": 116,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "B♭M7"
        },
        {
            "id": 117,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "B♭M7"
        },
        {
            "id": 118,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "CM7"
        },
        {
            "id": 119,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "CM7"
        },
        {
            "id": 120,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "CM7"
        },
        {
            "id": 121,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "C♯M7"
        },
        {
            "id": 122,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "C♯M7"
        },
        {
            "id": 123,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "C♯M7"
        },
        {
            "id": 124,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "DM7"
        },
        {
            "id": 125,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "DM7"
        },
        {
            "id": 126,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "DM7"
        },
        {
            "id": 127,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "D♭M7"
        },
        {
            "id": 128,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "D♭M7"
        },
        {
            "id": 129,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "D♭M7"
        },
        {
            "id": 130,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "D♯M7"
        },
        {
            "id": 131,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "D♯M7"
        },
        {
            "id": 132,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "D♯M7"
        },
        {
            "id": 133,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "EM7"
        },
        {
            "id": 134,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "EM7"
        },
        {
            "id": 135,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "EM7"
        },
        {
            "id": 136,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "E♭M7"
        },
        {
            "id": 137,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "E♭M7"
        },
        {
            "id": 138,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "E♭M7"
        },
        {
            "id": 139,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "FM7"
        },
        {
            "id": 140,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "FM7"
        },
        {
            "id": 141,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "FM7"
        },
        {
            "id": 142,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "F♯M7"
        },
        {
            "id": 143,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "F♯M7"
        },
        {
            "id": 144,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "F♯M7"
        },
        {
            "id": 145,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "GM7"
        },
        {
            "id": 146,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "GM7"
        },
        {
            "id": 147,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "GM7"
        },
        {
            "id": 148,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "G♭M7"
        },
        {
            "id": 149,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "G♭M7"
        },
        {
            "id": 150,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "G♭M7"
        },
        {
            "id": 151,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "maj7",
            "valueId": 1,
            "notation": "G♯M7"
        },
        {
            "id": 152,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "maj7",
            "valueId": 2,
            "notation": "G♯M7"
        },
        {
            "id": 153,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "maj7",
            "valueId": 4,
            "notation": "G♯M7"
        },
        {
            "id": 154,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "min",
            "valueId": 1,
            "notation": "Am"
        },
        {
            "id": 155,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "min",
            "valueId": 2,
            "notation": "Am"
        },
        {
            "id": 156,
            "rootNoteId": "a",
            "enharmonicNoteId": "a",
            "typeId": "min",
            "valueId": 4,
            "notation": "Am"
        },
        {
            "id": 157,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "min",
            "valueId": 1,
            "notation": "A♭m"
        },
        {
            "id": 158,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "min",
            "valueId": 2,
            "notation": "A♭m"
        },
        {
            "id": 159,
            "rootNoteId": "af",
            "enharmonicNoteId": "af",
            "typeId": "min",
            "valueId": 4,
            "notation": "A♭m"
        },
        {
            "id": 160,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "min",
            "valueId": 1,
            "notation": "A♯m"
        },
        {
            "id": 161,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "min",
            "valueId": 2,
            "notation": "A♯m"
        },
        {
            "id": 162,
            "rootNoteId": "as",
            "enharmonicNoteId": "bf",
            "typeId": "min",
            "valueId": 4,
            "notation": "A♯m"
        },
        {
            "id": 163,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "min",
            "valueId": 1,
            "notation": "Bm"
        },
        {
            "id": 164,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "min",
            "valueId": 2,
            "notation": "Bm"
        },
        {
            "id": 165,
            "rootNoteId": "b",
            "enharmonicNoteId": "b",
            "typeId": "min",
            "valueId": 4,
            "notation": "Bm"
        },
        {
            "id": 166,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "min",
            "valueId": 1,
            "notation": "B♭m"
        },
        {
            "id": 167,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "min",
            "valueId": 2,
            "notation": "B♭m"
        },
        {
            "id": 168,
            "rootNoteId": "bf",
            "enharmonicNoteId": "bf",
            "typeId": "min",
            "valueId": 4,
            "notation": "B♭m"
        },
        {
            "id": 169,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "min",
            "valueId": 1,
            "notation": "Cm"
        },
        {
            "id": 170,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "min",
            "valueId": 2,
            "notation": "Cm"
        },
        {
            "id": 171,
            "rootNoteId": "c",
            "enharmonicNoteId": "c",
            "typeId": "min",
            "valueId": 4,
            "notation": "Cm"
        },
        {
            "id": 172,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "min",
            "valueId": 1,
            "notation": "C♯m"
        },
        {
            "id": 173,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "min",
            "valueId": 2,
            "notation": "C♯m"
        },
        {
            "id": 174,
            "rootNoteId": "cs",
            "enharmonicNoteId": "df",
            "typeId": "min",
            "valueId": 4,
            "notation": "C♯m"
        },
        {
            "id": 175,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "min",
            "valueId": 1,
            "notation": "Dm"
        },
        {
            "id": 176,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "min",
            "valueId": 2,
            "notation": "Dm"
        },
        {
            "id": 177,
            "rootNoteId": "d",
            "enharmonicNoteId": "d",
            "typeId": "min",
            "valueId": 4,
            "notation": "Dm"
        },
        {
            "id": 178,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "min",
            "valueId": 1,
            "notation": "D♭m"
        },
        {
            "id": 179,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "min",
            "valueId": 2,
            "notation": "D♭m"
        },
        {
            "id": 180,
            "rootNoteId": "df",
            "enharmonicNoteId": "df",
            "typeId": "min",
            "valueId": 4,
            "notation": "D♭m"
        },
        {
            "id": 181,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "min",
            "valueId": 1,
            "notation": "D♯m"
        },
        {
            "id": 182,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "min",
            "valueId": 2,
            "notation": "D♯m"
        },
        {
            "id": 183,
            "rootNoteId": "ds",
            "enharmonicNoteId": "ef",
            "typeId": "min",
            "valueId": 4,
            "notation": "D♯m"
        },
        {
            "id": 184,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "min",
            "valueId": 1,
            "notation": "Em"
        },
        {
            "id": 185,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "min",
            "valueId": 2,
            "notation": "Em"
        },
        {
            "id": 186,
            "rootNoteId": "e",
            "enharmonicNoteId": "e",
            "typeId": "min",
            "valueId": 4,
            "notation": "Em"
        },
        {
            "id": 187,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "min",
            "valueId": 1,
            "notation": "E♭m"
        },
        {
            "id": 188,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "min",
            "valueId": 2,
            "notation": "E♭m"
        },
        {
            "id": 189,
            "rootNoteId": "ef",
            "enharmonicNoteId": "ef",
            "typeId": "min",
            "valueId": 4,
            "notation": "E♭m"
        },
        {
            "id": 190,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "min",
            "valueId": 1,
            "notation": "Fm"
        },
        {
            "id": 191,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "min",
            "valueId": 2,
            "notation": "Fm"
        },
        {
            "id": 192,
            "rootNoteId": "f",
            "enharmonicNoteId": "f",
            "typeId": "min",
            "valueId": 4,
            "notation": "Fm"
        },
        {
            "id": 193,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "min",
            "valueId": 1,
            "notation": "F♯m"
        },
        {
            "id": 194,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "min",
            "valueId": 2,
            "notation": "F♯m"
        },
        {
            "id": 195,
            "rootNoteId": "fs",
            "enharmonicNoteId": "gf",
            "typeId": "min",
            "valueId": 4,
            "notation": "F♯m"
        },
        {
            "id": 196,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "min",
            "valueId": 1,
            "notation": "Gm"
        },
        {
            "id": 197,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "min",
            "valueId": 2,
            "notation": "Gm"
        },
        {
            "id": 198,
            "rootNoteId": "g",
            "enharmonicNoteId": "g",
            "typeId": "min",
            "valueId": 4,
            "notation": "Gm"
        },
        {
            "id": 199,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "min",
            "valueId": 1,
            "notation": "G♭m"
        },
        {
            "id": 200,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "min",
            "valueId": 2,
            "notation": "G♭m"
        },
        {
            "id": 201,
            "rootNoteId": "gf",
            "enharmonicNoteId": "gf",
            "typeId": "min",
            "valueId": 4,
            "notation": "G♭m"
        },
        {
            "id": 202,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "min",
            "valueId": 1,
            "notation": "G♯m"
        },
        {
            "id": 203,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "min",
            "valueId": 2,
            "notation": "G♯m"
        },
        {
            "id": 204,
            "rootNoteId": "gs",
            "enharmonicNoteId": "af",
            "typeId": "min",
            "valueId": 4,
            "notation": "G♯m"
        }
    ]
};

export default staticData;
