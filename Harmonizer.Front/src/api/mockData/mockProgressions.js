export const progressions = [
  {
    "id": 239,
    "title": "Come as you are",
    "description": "Chorus of Nirvana's track",
    "chordsIds": [3, 40, 16, 45],
    "tempoId": 70
  }, {
    "id": 240,
    "title": "In a Sentimental Mood",
    "description": "by Duke Ellington",
    "chordsIds": [9, 23],
    "tempoId": 70
  }
];
const randomizer = (min, max) => {return Math.floor(Math.random() * (max - min) + min);}
export const progressionDetails = (id) => {
  var randArray = [];
  let nbBarsMin = 2;
  let nbBarsMax = 5;
  let nbItemsPerBarMin = 1;
  let nbItemsPerBarMax = 4;
  var nbBars = randomizer(nbBarsMin, nbBarsMax);
  for(var i=0; i< nbBars; i++){
    let barArray = [];
    let nbItemsPerBar = randomizer(nbItemsPerBarMin, nbItemsPerBarMax);
    for (var j = 0; j < nbItemsPerBar; j++) {
      barArray.push(randomizer(1, 200));
    }
    randArray.push(barArray);
  }
  return {
    "id": +id,
    "title": "premiere progression (modifiée)",
    "description": "premiere description (modifiée)",
    "barsChordsIds": randArray,
    "tempoId": 70
  }
};

export const progressionDetailsStatic = (id) => {
  return {
    "id": +id,
    "title": "premiere progression (modifiée)",
    "description": "premiere description (modifiée)",
    "barsChordsIds": [
        [
            3
        ],
        [
            2,
            5
        ],
        [
            7,
            10,
            5
        ]
    ],
    "tempoId": 70
  };
}
