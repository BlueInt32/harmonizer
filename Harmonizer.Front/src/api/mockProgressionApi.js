import delay from './delay';
import { progressionDetails, progressionDetailsStatic, progressions } from './mockData/mockProgressions';

// This file mocks a web API by working with the hard-coded data below. It uses
// setTimeout to simulate the delay of an AJAX call. All calls return promises.
//This would be performed on the server in a real app. Just stubbing in.
const generateId = (progression) => {
  let min = 200,
    max = 90000000;
  return Math.random() * (max - min) + min;
};

class ProgressionApi {
  static getAllProgressions() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], progressions));
      }, delay);
    });
  }

  static getProgressionDetails(id) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        //resolve(Object.assign({}, progressionDetails(id)));
        resolve(Object.assign({}, progressionDetailsStatic(id)));
      }, delay);
    });
  }

  static saveProgression(progression) {
    progression = Object.assign({}, progression); // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minCourseTitleLength = 1;
        if (progression.title.length < minCourseTitleLength) {
          reject(`Title must be at least ${minCourseTitleLength} characters.`);
        }

        if (progression.id) {
          const existingCourseIndex = progressions.findIndex(a => a.id === progression.id);
          progressions.splice(existingCourseIndex, 1, progression);
        } else {
          // Just simulating creation here. The server would generate ids and watchHref's
          // for new progressions in a real app. Cloning so copy returned is passed by
          // value rather than by reference.
          progression.id = generateId(progression);
          progressions.push(progression);
        }

        resolve(progression);
      }, delay);
    });
  }

  static deleteProgression(courseId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfCourseToDelete = progressions.findIndex(progression => {
          return progression.id === courseId;
        });
        progressions.splice(indexOfCourseToDelete, 1);
        resolve();
      }, delay);
    });
  }
}

export default ProgressionApi;
