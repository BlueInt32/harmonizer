import delay from './delay';
import staticData from './mockData/mockStaticData';

class StaticDataApi {
  static getAllStaticData() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign({}, staticData));
      }, delay);
    });
  }
}

export default StaticDataApi;
