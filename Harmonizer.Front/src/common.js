export function isEmpty(obj) {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
}
export function updateObject(oldObject, newValues) {
  // Encapsulate the idea of passing a new object as the first parameter
  // to Object.assign to ensure we correctly copy data instead of mutating
  return Object.assign({}, oldObject, newValues);
}

export function deepCloneArray(cloned) {
  var arr = [];
  for( var i = 0; i < cloned.length; i++ ) {
    if(Array.isArray(cloned[i])) {
      arr[i] = deepCloneArray(cloned[i]);
      continue;
    }
    if(typeof cloned[i] === 'object'){
      arr[i] = {...cloned[i]};
      continue;
    }
    arr[i] = cloned[i];
  }
  return arr;
}

export function findChordFromChange(previousChord, staticDataChords, delta) {
  let updatedChord = updateObject(previousChord, delta);
  let correspondingStaticChord = staticDataChords.filter(c =>
    c.rootNoteId === updatedChord.rootNoteId
    && c.typeId === updatedChord.typeId
    && c.valueId === updatedChord.valueId
  )[0];
  return updateObject(updatedChord, correspondingStaticChord);
}
