import a from './a_sprite.mp3';
import af from './af_sprite.mp3';
import b from './b_sprite.mp3';
import bf from './bf_sprite.mp3';
import c from './c_sprite.mp3';
import d from './d_sprite.mp3';
import df from './df_sprite.mp3';
import e from './e_sprite.mp3';
import ef from './ef_sprite.mp3';
import f from './f_sprite.mp3';
import g from './g_sprite.mp3';
import gf from './gf_sprite.mp3';
import met from './metronome.mp3';

export default {
  'a': a,
  'af': af,
  'b': b,
  'bf': bf,
  'c': c,
  'd': d,
  'df': df,
  'e': e,
  'ef': ef,
  'f': f,
  'g': g,
  'gf': gf,
  'met': met
};
