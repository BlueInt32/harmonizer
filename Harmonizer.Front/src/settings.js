export default {
  useMock: true,
  barLength: 4,
  colors: {
    header: "primaryColor",
    headerText: "",
    background: "backgroundColor",
    chordBoxBackground: "chordBoxBackgroundColor",
    ctaButton: "ctaColor",
  },
  shadowDepth: {
    header: "z-depth-1",
  },
  player: {
    spriteSegmentDuration: 3600,
    antiNextTransientTime: 10,
    lengthComputer: (valueId, tempoId) => { return valueId * 60000 / tempoId; }
  }
};
