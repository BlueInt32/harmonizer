import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { withRouter } from 'react-router-dom'
import thunk from 'redux-thunk';
import multi from 'redux-multi';
import { createStore, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { loadStaticData } from './actions/staticDataActions';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux'
import rootReducer from './reducers';
import createLogger from 'redux-logger';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import MainContainer from './components/MainContainer';
import whenMiddleWare from 'redux-when';
import Player from './Player';

const history = createHistory();

const middleWares = [
  routerMiddleware(history), // place routing stuff in the store state
  thunk, // functions can be dispatched as actions
  multi, // arrays of functions can be dispatched in place of single actions
  reduxImmutableStateInvariant(), // throws when trying to change a state
  whenMiddleWare // dispatch an action when state satisfies a given criteria
];
if (process.env.NODE_ENV !== 'production') {
  middleWares.push(createLogger({
    collapsed: (getState, action, logEntry) => !logEntry.error
  }));
}

const store = createStore(rootReducer, applyMiddleware(...middleWares) );
store.dispatch(loadStaticData());
new Player(store);

const RoutedMainContainer = withRouter(MainContainer);
ReactDOM.render(
  <Provider store={ store }>
    <ConnectedRouter history={ history }>
      <RoutedMainContainer/>
    </ConnectedRouter>
  </Provider>, document.getElementById('root')
);
