import expect from 'expect';
import React from 'react';
import { ProgressionSettingsPage } from './ProgressionSettingsPage';
import { mount, shallow } from 'enzyme';
import { updateObject } from '../../common';
import sinon from 'sinon';

describe('ProgressionSettingsPage', () => {
  const spy = sinon.spy();
  const baseState = {
    params: {
      id: '233'
    },
    progressionContext: {
      id: '',
      watchHref: '',
      title: '',
      authorId: '',
      chords: [],
      editedChord: {
        commitPosition: 0,
        noteId: "",
        typeId: "maj",
        valueId: 2
      }
    },
    staticData: {
      defaultTempoId: 100,
      defaultNoteId: "c",
      defaultTypeId: "maj",
      defaultValueId: 2,
      notes: [],
      chordTypes: [],
      values: []
    }
  };
  it('button displays Save while not saving', () => {
    const state = updateObject(baseState, {});
    const baseStore = {
      subscribe: () => {},
      dispatch: () => {},
      getState: () => ({...state})
    };
    const mountOptions = {
      context: { store: baseStore },
      childContextTypes: { store: React.PropTypes.object.isRequired }
    };
    const wrapper = mount(<ProgressionSettingsPage {...baseState} />, mountOptions);
    expect(wrapper.find('#saveSettings').length).toBe(1);
    expect(wrapper.find('#saveSettings').contains(['Save'])).toBe(true);
  });
});
