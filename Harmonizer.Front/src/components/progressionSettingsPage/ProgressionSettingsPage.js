import React, { Component } from 'react';
import TextInput from '../common/TextInput';
import TextAreaInput from '../common/TextAreaInput';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as settingsActions from '../../actions/settingsActions';
import { Button } from 'react-bootstrap';
import settings from '../../settings';
import * as routeActions from 'react-router-redux';

export class ProgressionSettingsPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.updateProgressionState = this.updateProgressionState.bind(this);
    this.cancelEdition = this.cancelEdition.bind(this);
    this.commitEdition = this.commitEdition.bind(this);
  }
  componentWillMount(){
    this.setState({
      progression: Object.assign({}, this.props.progression)
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.progression.id !== nextProps.progression.id) {
      this.setState({
        progression: Object.assign({}, nextProps.progression)
      });
    }
  }

  updateProgressionState(event) {
    const field = event.target.name;
    let progression = this.state.progression;
    progression[field] = event.target.value;

    return this.setState({
      progression: progression
    });
  }

  commitEdition(){
    this.props.settingsActions.commitSettingsEdition({
      newTitle: this.state.progression.title,
      newDescription: this.state.progression.description
    });
    this.props.routeActions.push(`/progression/${this.state.progression.id}`);
    //browserHistory.push('/progression/' + this.state.progression.id);
  }
  cancelEdition(){
    this.props.settingsActions.cancelSettingsEdition();
    this.props.routeActions.push(`/progression/${this.state.progression.id}`);
    //browserHistory.push('/progression/' + this.state.progression.id);
  }

  render() {
    return (
      <div>
        <TextInput
          name="title"
          label="title"
          placeHolder="Progression title"
          value={ this.state.progression.title }
          onChange={ this.updateProgressionState } />
        <TextAreaInput
          name="description"
          placeHolder="Progression description"
          label="description"
          value={ this.state.progression.description }
          onChange={ this.updateProgressionState } />
        <Button flat large id="cancel" onClick={ this.cancelEdition }>
          Cancel
        </Button>
        <Button large id="saveSettings" waves="light" className={ settings.colors.cta } onClick={ this.commitEdition }>
          Save
        </Button>
      </div >
      );
  }
}

const mapStateToProps = (state, {params}) => {
  return {progression: Object.assign({}, state.progressionContext)};
};

function mapDispatchToProps(dispatch) {

  return {
    settingsActions: bindActionCreators(settingsActions, dispatch),
    routeActions: bindActionCreators(routeActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProgressionSettingsPage);
