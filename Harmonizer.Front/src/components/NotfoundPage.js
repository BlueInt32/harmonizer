import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class NotFoundPage extends Component {
  render() {
    return (
      <div>
        <h1>404</h1>
        <p>It seems you're lost !</p>
        <p>
          <Link to="home">Go back to Home Page</Link>
        </p>
      </div>
    );
  }
}

export default NotFoundPage
