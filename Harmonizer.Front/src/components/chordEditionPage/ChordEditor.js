import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import * as chordActions from '../../actions/chordActions';
import * as playerActions from '../../actions/playerActions';
import * as apiActions from '../../actions/apiActions';
import * as progressionContextActions from '../../actions/progressionContextActions';
import { connect } from 'react-redux';
//import { browserHistory } from 'react-router';
import { isEmpty } from '../../common';
import Switch from '../common/switch';
import * as routeActions from 'react-router-redux';

export class ChordEditor extends Component {
  constructor(props, context) {
    super(props, context);
    this.switchSharpsFlat = this.switchSharpsFlat.bind(this);
    this.reduceNotesAccordingToFlatOrSharp = this.reduceNotesAccordingToFlatOrSharp.bind(this);
    this.state = {
      flatOrSharp: 'flat',
      allNotes: [],
      visibleNotes: [],
      chordTypes: [],
      values: []
    };

    // bindings of this
    this.changeChordNote = this.changeChordNote.bind(this);
    this.changeChordType = this.changeChordType.bind(this);
    this.changeChordValue = this.changeChordValue.bind(this);
    this.saveChordToProgression = this.saveChordToProgression.bind(this);
    this.cancelChordEdition = this.cancelChordEdition.bind(this);
  }

  componentWillMount(){

    console.log("here in chordEditor")
    if(isEmpty(this.props.editedChord)) {
      console.log("editedChord", this.props.editedChord);
      this.props.routeActions.push(`/progression/${this.props.match.params.progressionId}`);
      return;
    }else {
      console.log("editedChord", this.props.editedChord);

    }
    if(isEmpty(this.props.staticData)){
      return;
    }
    this.setState({
      allNotes: this.props.staticData.notes,
      chordTypes:this.props.staticData.chordTypes,
      values: this.props.staticData.values
    });
    let alterationToggle = 'sharp';
    if(!isEmpty(this.props.editedChord) && this.props.editedChord.rootNoteId.indexOf('s') > 0) {
      alterationToggle = 'flat';
    }

    this.reduceNotesAccordingToFlatOrSharp(this.props.staticData.notes, alterationToggle);
  }

  componentWillReceiveProps(nextProps){

  }

  switchSharpsFlat(event){
    console.log("SWITCHING !")
    this.reduceNotesAccordingToFlatOrSharp(this.state.allNotes, this.state.flatOrSharp);
    if(this.state.flatOrSharp === 'flat') {
      this.setState({flatOrSharp:'sharp'});
    }
    else {
      this.setState({flatOrSharp:'flat'});
    }
  }

  reduceNotesAccordingToFlatOrSharp(notes, previousAlteration){
    let visibleNotes = notes.filter(n => {
      if(n.id.length === 1) // we force show notes that do not have alterations
        return true;
      if(previousAlteration === 'sharp' && n.id.indexOf('s') === -1){
        return true;
      } else if(previousAlteration === 'flat' && (n.id.indexOf('f') === -1 || n.id === 'fs')){
        return true;
      }
      return false;
    });

    this.setState({visibleNotes: visibleNotes});
  }

  changeChord(modifier){
    if(this.props.playerData.isPlaying) {
      this.props.playerActions.stopPlaying();
    }
    this.props.chordActions.changeEditedChordComposite(modifier);

  }
  changeChordNote(rootNoteId){
    this.changeChord({rootNoteId});
  }

  changeChordType(typeId){
    this.changeChord({typeId});
  }

  changeChordValue(valueId){
    this.changeChord({valueId});
  }

  saveChordToProgression() {
    this.props.progressionContextActions.replaceBarInProgression();
    this.props.routeActions.push(`/progression/${this.props.match.params.progressionId}`);
  }

  cancelChordEdition() {
    this.props.chordActions.replaceEditedChord({});
    this.props.routeActions.push(`/progression/${this.props.match.params.progressionId}`);
  }

  render(){
    let editedChord = this.props.editedChord;
    let notes = this.state.visibleNotes;
    let chordTypes = this.state.chordTypes;
    let values = this.state.values;
    return (
      <div className="BodyLayout-chordEditor ChordEditor">
        <h2>{editedChord.notation}</h2>
        <div className="ChordEditor-attributes row">
          <div className="col-4">
            <Switch onChange={this.switchSharpsFlat} value={this.state.flatOrSharp}></Switch>
            <ul className="list-group ChordEditor-notesList">
              {
                notes.map(note =>
                  <li key={note.id}
                    className={"list-group-item text-center d-inline-block " + (editedChord.rootNoteId === note.id ? "active": "") }
                    onClick={() => { this.changeChordNote(note.id);}}>
                    {note.name}
                  </li>
                )
              }
            </ul>
          </div>
          <div className="col-4">
            <ul className="list-group ChordEditor-typesList">
              {
                chordTypes.map(type =>
                  <li key={type.id}
                    className={"list-group-item text-center d-inline-block " + (editedChord.typeId === type.id ? "active" : "")}
                      onClick={() => { this.changeChordType(type.id);} }>
                      {type.notation || <span style={{ color : 'grey', }}>(major)</span>}
                  </li>
                )
              }
            </ul>
          </div>
          <div className="col-4">
            <ul className="list-group ChordEditor-valuesList">
              {
                values.map(value =>
                  <li key={value.id}
                  className={"list-group-item text-center d-inline-block " + (editedChord.valueId === value.id ? "active" : "")}
                  onClick={() => { this.changeChordValue(value.id); }}
                  >{value.name}</li>
                )
              }
            </ul>
          </div>
        </div>
        <button className={"btn btn-secondary"} onClick={this.cancelChordEdition}>Cancel</button>
        <button className={"btn btn-primary" } onClick={this.saveChordToProgression}>Save</button>
      </div>
      );
  }
}
const mapStateToProps = (state, {params}) => {
  return {
    appState: state.appState,
    progressionContext: state.progressionContext,
    staticData: state.staticData,
    playerData: state.playerData,
    editedChord: state.editedChord
  }
};

function mapDispatchToProps(dispatch) {
  return {
    progressionContextActions: bindActionCreators(progressionContextActions, dispatch),
    chordActions: bindActionCreators(chordActions, dispatch),
    playerActions: bindActionCreators(playerActions, dispatch),
    apiActions: bindActionCreators(apiActions, dispatch),
    routeActions: bindActionCreators(routeActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ChordEditor);
