import React, { Component } from 'react';
import Overlay from './common/Overlay';
import Header from './header/Header';
import Footer from './header/Footer';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as apiActions from '../actions/apiActions';
import { push } from 'react-router-redux';
import "../css/bootstrap-4.0.0-alpha.6/scss/bootstrap.scss";
import "../css/app.scss";
import "../vendor/jquery-3.0.0.slim.js";
import "../css/bootstrap-4.0.0-alpha.6/js/src/collapse.js";
import "../css/font-awesome-4.7.0/scss/font-awesome.scss"
import Routing from './Routing';

class MainContainer extends Component {
  render() {
    return (
      <div className="harmonizerContainer">
        <Overlay active={this.props.loading}></Overlay>
        <Header
          loading={ this.props.loading }
          saveProgression={ this.props.apiActions.saveProgression }
          title={"Harmonizer"} />
        <div className="BodyLayout">
          <Routing />
        </div>
        <Footer />
      </div>
      );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    location: state.router.location
  }
}
function mapDispatchToProps(dispatch) {
  let boundApiActions = bindActionCreators(apiActions, dispatch);
  let pushRouteActions = bindActionCreators(push, dispatch);

  return {
    apiActions: boundApiActions,
    push: pushRouteActions
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
