import React from 'react';
import HomePage from './homePage/HomePage';
import ProgressionPage from './progressionPage/ProgressionPage';
import BrowserPage from './browserPage/BrowserPage';
import NotFoundPage from './NotfoundPage';
import ProgressionSettingsPage from './progressionSettingsPage/ProgressionSettingsPage';
import ChordEditor from './chordEditionPage/ChordEditor';
import { Route, Switch } from 'react-router-dom';

const Routing = () => {

  return (<Switch>
            <Route path='/browse' component={ BrowserPage } />
            <Route path='/progression/:progressionId/chord' component={ ChordEditor } />
            <Route path='/progression/:progressionId/details' component={ ProgressionSettingsPage } />
            <Route path='/progression/:progressionId' component={ ProgressionPage } />
            <Route path='/progression' component={ ProgressionPage } />
            <Route path='/' component={ HomePage } />
            <Route component={ NotFoundPage } />
          </Switch>
  );
}
export default Routing;
