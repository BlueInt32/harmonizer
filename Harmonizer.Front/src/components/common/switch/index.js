import React from 'react';

const Switch = ({onChange, value}) => {
  let flatActive = value === 'flat';
  let flatBtnStyle = flatActive ? "btn-primary" : "btn-secondary";
  let sharpActive = value === 'sharp';
  let sharpBtnStyle = sharpActive ? "btn-primary" : "btn-secondary";
  return (

  <div className="btn-group btn-group-justified mb-2" role="group" aria-label="Basic example">
    <div className="btn-group" role="group">
      <button type="button" className={"btn " + flatBtnStyle} onClick={onChange}>♭</button>
    </div>
    <div className="btn-group" role="group">
      <button type="button" className={"btn " + sharpBtnStyle} onClick={onChange}>♯</button>
    </div>
  </div>
)
}

export default Switch;
