
import React from 'react';

const Overlay = ({active}) => {
  return (
    <div className="Overlay" style={{display: active ? "visible": "none"}}><p className="overlay-content">Loading...</p></div>
  )
}

export default Overlay;
