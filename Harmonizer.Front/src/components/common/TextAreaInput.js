import React from 'react';

class TextInput extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.props.onChange(e);
  }

  render() {
    let value = this.props.value || "";
    let wrapperClass = 'form-group';
    if (this.props.error && this.props.error.length > 0) {
      wrapperClass += " has-error";
    }
    return (
      <div className={ wrapperClass }>
        <div className="input-field col 6">
          <textarea rows="4" type="text" name={ this.props.name } className="materialize-textarea" placeholder={ this.props.placeHolder } value={ value } onChange={ this.onChange }
          />
          { this.props.error && <div className="alert alert-danger">
                                  { this.props.error }
                                </div> }
        </div>
      </div>
      );
  }
}

export default TextInput;
