import React from 'react';

const ProgressionCommands = (
    {
      startPlaying,
      stopPlaying,
      editDetails,
      toggleEditMode,
      toggleMetronome,
      metronomeIsActive,
      editModeIsActive
    }) => {
      let editionButton = <button type="button" className="btn btn-secondary" onClick={toggleEditMode}>
                <i className="fa fa-pencil"></i> Edit mode
              </button>
      if(editModeIsActive){
        editionButton = <button type="button" className="btn btn-secondary" onClick={toggleEditMode}>
                <i className="fa fa-music"></i> Play mode
              </button>
      }
  return (
    <div className="Progression-commands Commands btn-toolbar">
      <form className="form-inline"  action="">
        <div className="form-group">
          <div className="btn-group mr-4">
              <button type="button" className="btn btn-primary" onClick={startPlaying}>
                <i className="fa fa-play"></i> Play
              </button>
              <button type="button" className="btn btn-secondary" onClick={stopPlaying}>
                <i className="fa fa-stop"></i> Stop
              </button>
              <button type="button" className="btn btn-secondary" onClick={editDetails}>
                <i className="fa fa-cog"></i> Settings
              </button>
              {editionButton}
          </div>
          <label className="custom-control custom-checkbox" >
            <input type="checkbox" className="custom-control-input" onChange={toggleMetronome} checked={metronomeIsActive} />
            <span className="custom-control-indicator"></span>
            <span className="custom-control-description">Metronome</span>
          </label>
        </div>
      </form>
    </div>
  );
};

export default ProgressionCommands;
