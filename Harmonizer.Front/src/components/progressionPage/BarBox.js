import React from 'react';
import ChordBox from './ChordBox';
const createLocalization = (barIndex,chordIndex, chordPositioning) => {
  return {
    addBar: null,
    barIndex,
    addChordInBar:chordPositioning,
    chordIndex
  };
};
const BarBox = ({
    bar,
    barIndex,
    onClickEditHandler,
    onClickDeleteHandler,
    onClickAddLeftHandler,
    onClickAddRightHandler,
    isWholeProgressionPlaying,
    editMode,
    clickChordHandler
  }) => {
  return (
    <div className="BarBox col-12 col-md-6 col-xl-4">
      <div className="card-group">
        {
        bar.map((chord, chordIndex) =>
          <ChordBox
            key={ chordIndex }
            chord={ chord }
            onClickEditHandler={onClickEditHandler}
            onClickDeleteHandler={onClickDeleteHandler.bind(null, barIndex, chordIndex)}
            onClickAddLeftHandler={onClickAddLeftHandler.bind(null, createLocalization(barIndex, chordIndex, 'left'))}
            onClickAddRightHandler={onClickAddRightHandler.bind(null, createLocalization(barIndex, chordIndex, 'right'))}
            isWholeProgressionPlaying={isWholeProgressionPlaying}
            editMode={editMode}
            clickChordHandler={clickChordHandler.bind(null, chord)}/>
          )
        }
      </div>
    </div>
    );
};

export default BarBox;
