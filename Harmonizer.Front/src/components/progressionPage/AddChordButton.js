import React from 'react';

const AddChord = ({onClick}) => {
  return (
    <div className="card ChordsList-addChord AddChord" onClick={ onClick }>
      <div className="card-block"><h1>+</h1></div>
    </div>
  );
}

export default AddChord;
