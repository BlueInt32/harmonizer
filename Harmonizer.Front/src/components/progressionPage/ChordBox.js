import React from 'react';

const ChordBox = ({
    chord,
    onClickEditHandler,
    onClickDeleteHandler,
    onClickAddLeftHandler,
    onClickAddRightHandler,
    isWholeProgressionPlaying,
    editMode,
    clickChordHandler
  }) => {
  let commands = null,
    addLeftButton = null,
    addRightButton = null;
  if(editMode) {
    if(chord.valueId > 1) {
      addLeftButton = <button type="button" className="btn btn-secondary btn-sm" onClick={onClickAddLeftHandler} title="edit the chord">
                        <i className="fa fa-plus-square-o"/>
                      </button>
      addRightButton =<button type="button" className="btn btn-secondary btn-sm" onClick={onClickAddRightHandler} title="edit the chord">
                        <i className="fa fa-plus-square-o"/>
                      </button>
    }
    commands =  <div className="ChordBox-chordCommands">
                  {addLeftButton}
                  <button type="button" className="btn btn-secondary btn-sm" onClick={onClickEditHandler.bind(null, chord._localization)} title="edit the chord">
                    <i className="fa fa-edit"/>
                  </button>
                  <button type="button" className="btn btn-secondary btn-sm"  onClick={onClickDeleteHandler.bind(null, chord._localization)} title="remove the chord">
                    <i className="fa fa-trash"/>
                  </button>
                  {addRightButton}
                </div>
  }
  return (
    <div
      className={ "card ChordsList-chordBox" }
      style={{flexGrow:chord.valueId}}
      onClick={clickChordHandler}>
      <div
        className={"ChordBox " + (chord.isPlaying ? "card-primary" : "")}
        draggable="true">
          <h4 className="ChordBox-name display-5">{ chord.notation }</h4>
          {commands}
        </div>
      </div>
      );
  };

  export default ChordBox;
