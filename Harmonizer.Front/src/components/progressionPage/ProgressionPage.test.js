import expect from 'expect';
import React from 'react';
import { ProgressionPage } from './ProgressionPage';
import { mount, shallow } from 'enzyme';
import { updateObject } from '../../common';
import sinon from 'sinon';


describe('Progression Page', () => {
  const spy = sinon.spy();
  const baseState = {
    params: {
      id: '233'
    },
    progressionContext: {
      id: '',
      watchHref: '',
      title: '',
      authorId: '',
      chords: [],
      editedChord: {
        commitPosition: 0,
        noteId: "",
        typeId: "maj",
        valueId: 2
      }
    },
    staticData: {
      defaultTempoId: 100,
      defaultNoteId: "c",
      defaultTypeId: "maj",
      defaultValueId: 2,
      notes: [],
      chordTypes: [],
      values: []
    },
    apiActions: {
      loadProgressionDetails: () => {
        return Promise.resolve({
          progressionContext: {
            chords: []
          }
        });
      }
    },
    chordActions: {
      startAddingChord: spy
    },
    playerActions: { startPlayingProgression: () => {}},
    progressionContextActions: {
      saveProgression: () => {
        return Promise.resolve();
      }
    },
    appState: {
      isEditingChord: false
    }
  };
  const testChords = [
    {
      "id": 866,
      "positionInSequence": 0,
      "chordId": 1,
      "chord": {
        "id": 1,
        "rootNoteId": "a",
        "rootNote": {
          "id": "a ",
          "name": "A"
        },
        "typeId": "dom7",
        "chordType": {
          "id": "dom7",
          "name": "Dominant Seventh",
          "notation": "7",
          "description": "",
          "spriteOffset": 7200
        },
        "valueId": 1,
        "value": {
          "id": 1,
          "name": "Quarter Note (1)"
        },
        "notation": "A7"
      }
    },
    {
      "id": 867,
      "positionInSequence": 1,
      "chordId": 4,
      "chord": {
        "id": 4,
        "rootNoteId": "ab",
        "rootNote": {
          "id": "ab",
          "name": "A♭"
        },
        "typeId": "dom7",
        "chordType": {
          "id": "dom7",
          "name": "Dominant Seventh",
          "notation": "7",
          "description": "",
          "spriteOffset": 7200
        },
        "valueId": 1,
        "value": {
          "id": 1,
          "name": "Quarter Note (1)"
        },
        "notation": "A♭7"
      }
    }
  ];
  it('should call \'startAddingChord\' action when clicking on \'add chord\' button', () => {
    const state = updateObject(baseState, {});
    const baseStore = {
      subscribe: () => {},
      dispatch: () => {},
      getState: () => ({...state})
    };
    const mountOptions = {
      context: { store: baseStore },
      childContextTypes: { store: React.PropTypes.object.isRequired }
    };
    const wrapper = mount(<ProgressionPage {...state} />, mountOptions);
    const addButton = wrapper.find('.btn-floating');
    addButton.simulate('click');
    expect(spy.calledOnce).toBe(true);
  });
  it('should contain an addButton', () => {

    const state = updateObject(baseState, {});
    const baseStore = {
      subscribe: () => {},
      dispatch: () => {},
      getState: () => (baseState)
    };
    const mountOptions = {
      context: { store: baseStore },
      childContextTypes: { store: React.PropTypes.object.isRequired }
    };
    const wrapper = mount(<ProgressionPage {...baseState} />, mountOptions);
    //const saveButton = wrapper.find('#saveProgression').last();
    //expect(saveButton.prop('type')).toBe('submit');
    //saveButton.simulate('click');
    const addButton = wrapper.find('.btn-floating');
    expect(addButton.length).toBe(1);
    expect(addButton.text()).toBe('add');
    //expect(wrapper.state().errors.title).toBe('Title must be at least 5 characters');
  });
  it('should display chordWrappers according to state\'s chords', () => {
    const state = updateObject(baseState, { progressionContext: updateObject(baseState.progressionContext, {chords: testChords})});
    const baseStore = {
      subscribe: () => {},
      dispatch: () => {},
      getState: () => (state)
    };
    const mountOptions = {
      context: { store: baseStore },
      childContextTypes: { store: React.PropTypes.object.isRequired }
    };

    const wrapper = mount(<ProgressionPage {...state} />, mountOptions);
    //const saveButton = wrapper.find('#saveProgression').last();
    //expect(saveButton.prop('type')).toBe('submit');
    //saveButton.simulate('click');
    const chordsWrappers = wrapper.find('.chordWrapper');
    expect(chordsWrappers.length).toBe(2);
    //expect(wrapper.state().errors.title).toBe('Title must be at least 5 characters');
  });
});
