import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as progressionContextActions from '../../actions/progressionContextActions';
import * as apiActions from '../../actions/apiActions';
import * as playerActions from '../../actions/playerActions';
import * as settingsActions from '../../actions/settingsActions';
import * as chordActions from '../../actions/chordActions';
import BarBox from './BarBox';
import AddChordButton from './AddChordButton';
import ProgressionCommmands from './ProgressionCommands';
import ChordEditor from '../chordEditionPage/ChordEditor';
import { isEmpty } from '../../common';
import * as routeActions from 'react-router-redux';

export class ProgressionPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.editDetails = this.editDetails.bind(this);
    this.editChord = this.editChord.bind(this);
    this.toggleMetronome = this.toggleMetronome.bind(this);
    this.startEditingChord = this.startEditingChord.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.progressionLoaded = false;
    this.clickChordHandler = this.clickChordHandler.bind(this);
    this.startAddingChord = this.startAddingChord.bind(this);
  }

  componentDidMount() {
    let shouldLoadProgression = this.shouldLoadProgressionFromRoute(this.props);
    if(shouldLoadProgression){

      this.progressionLoaded = true;
      this.props.apiActions.loadProgressionDetails(this.props.match.params.progressionId);
    }
  }

  componentWillReceiveProps(newProps) {
    let shouldLoadProgression = this.shouldLoadProgressionFromRoute(newProps);
    if(shouldLoadProgression) {
      this.progressionLoaded = true;
      newProps.apiActions.loadProgressionDetails(newProps.match.params.progressionId);
    }
  }

  shouldLoadProgressionFromRoute(props){
    let routeProgressionId = props.match.params.progressionId;
    let result = typeof routeProgressionId !== 'undefined'
      && +routeProgressionId !== props.progressionContext.id
      && !isEmpty(props.staticData)
      && !this.progressionLoaded;

    return result;
  }

  toggleMetronome(){
    this.props.playerActions.toggleMetronome();
  }

  progressionFormIsValid() {
    let formIsValid = true;
    let errors = { };

    if (this.props.progression.title.length < 2) {
      errors.title = 'Title must be at least 2 characters';
      formIsValid = false;
    }
    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  saveProgressionState(event) {
    event.preventDefault();
    if (!this.progressionFormIsValid()) {
      return;
    }
    this.setState({
      saving: true
    });
    this.props.progressionContextActions.saveProgression(this.props.progression)
      .then(() => {
        this.context.router.push('/browse');
        this.redirect();
      })
      .catch(error => {
        this.setState({
          saving: false
        });
        alert(error);
      });
  }

  editDetails() {
    this.props.settingsActions.startSettingsEdition();
    this.props.routeActions.push(`/progression${this.props.progressionContext.id}details`);
  }

  toggleEditMode() {
    this.props.settingsActions.toggleEditMode();
  }

  editChord(index) {
    //this.props.
  }

  redirect() {
    this.context.router.push('/browse');
    alert('Progression saved !');
  }

  clickChordHandler(chord) {
    this.props.playerActions.startPlayingChordInProgression(chord);
  }

  startEditingChord(chordLocalization) {
    this.props.chordActions.startChordEditionComposite(this.props.match.params.progressionId, chordLocalization);
  }

  splitChord() {

  }

  startAddingChord(chordLocalization) {
  }

  render() {
    let playerActions = this.props.playerActions;
    let progressionContextActions = this.props.progressionContextActions;

    return (
      <div className="BodyLayout-progression Progression">
        <ProgressionCommmands
          startPlaying={playerActions.startPlayingProgression}
          stopPlaying={playerActions.stopPlaying}
          editDetails={this.editDetails}
          toggleEditMode={this.toggleEditMode}
          toggleMetronome={this.toggleMetronome}
          metronomeIsActive={this.props.playerData.metronome}
          editModeIsActive={this.props.appState.editMode}
           />
        <div className="container-fluid Progression-chordsList ChordsList">
          <div className="row">
            {
              this.props.progressionContext._bars.map((bar, barIndex) =>
                    <BarBox key={ barIndex }
                      bar={ bar }
                      barIndex= { barIndex }
                      onClickEditHandler={this.startEditingChord}
                      onClickDeleteHandler={progressionContextActions.deleteChordFromProgression}
                      onClickAddLeftHandler={this.startAddingChord}
                      onClickAddRightHandler={this.startAddingChord}
                      isWholeProgressionPlaying={this.props.playerData.isPlaying}
                      editMode={this.props.appState.editMode}
                      clickChordHandler={this.clickChordHandler}
                    />
                )
            }
            <div className="col-xs-2 col-md-1">
              <AddChordButton  onClick={ this.startAddingChord.bind(null, `${this.props.progressionContext._bars.length},0`) } />
            </div>
          </div>
        {this.props.appState.isEditingChord ? (<ChordEditor />): (null) }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return Object.assign({}, state, ownProps.params);
};

function mapDispatchToProps(dispatch) {
  let boundApiActions = bindActionCreators(apiActions, dispatch);
  let boundProgressionContextActions = bindActionCreators(progressionContextActions, dispatch);
  let boundPlayerActions = bindActionCreators(playerActions, dispatch);
  let boundSettingsActions = bindActionCreators(settingsActions, dispatch);
  let boundChordActions = bindActionCreators(chordActions, dispatch);
  let boundRouteActions = bindActionCreators(routeActions, dispatch);

  return {
    apiActions: boundApiActions,
    progressionContextActions:boundProgressionContextActions,
    playerActions: boundPlayerActions,
    settingsActions: boundSettingsActions,
    chordActions: boundChordActions,
    routeActions: boundRouteActions
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgressionPage);
