import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as apiActions from '../../actions/apiActions';
import ProgressionsList from './ProgressionsList';
//import { browserHistory } from 'react-router';
import { Button } from 'react-bootstrap';
import settings from '../../settings';
import * as routeActions from 'react-router-redux';

class BrowserPage extends Component {
  constructor(props, context) {
    super(props, context);

    this.redirectToAddProgressionPage = this
      .redirectToAddProgressionPage
      .bind(this);
    this.onProgressionClick = this.onProgressionClick.bind(this);
  }

  redirectToAddProgressionPage() {
    this.props.routeActions.push('/progression');
  }
  componentWillMount(){
    this.props.apiActions.loadProgressions();
  }

  onProgressionClick(progressionId) {
    this.props.routeActions.push('/progression/' + progressionId);
  }

  render() {
    const progressions = this.props.progressionsList;
    return (
      <div className="section">
        <h1>Progressions</h1>
        <Button bsSize="large" className={ settings.colors.cta } onClick={ this.redirectToAddProgressionPage }>Add</Button>
        <ProgressionsList progressions={ progressions } onProgressionClick={ this.onProgressionClick } />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    progressionsList: state.progressionsList,
    location: state.router.location
  };
};

function mapDispatchToProps(dispatch) {
  var boundActions = bindActionCreators(apiActions, dispatch);
  var boundRouteActions = bindActionCreators(routeActions, dispatch);
  return {
    apiActions: boundActions,
    routeActions: boundRouteActions
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BrowserPage);
