import React from 'react';
import ProgressionListRow from './ProgressionListRow';

const ProgressionsList = ({progressions, onProgressionClick}) => {
  return (
    <ul className="collection">
      { progressions.map(progression => <ProgressionListRow key={ progression.id } progression={ progression } onProgressionClick={ onProgressionClick } />
        ) }
    </ul>
    );
};

export default ProgressionsList;
