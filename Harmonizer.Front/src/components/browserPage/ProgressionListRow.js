import React from 'react';
import settings from '../../settings';
import { Button } from 'react-bootstrap';

const ProgressionListRow = ({progression, onProgressionClick}) => {
  function onLocalClick() {
    onProgressionClick(progression.id);
  }
  return (
    <li className="collection-item" onClick={ onLocalClick }>
      <h5 className="title">{ progression.title } </h5>
      <p>
        { progression.description }
      </p>
      <Button className={ settings.colors.cta } onClick={ onLocalClick}>Edit</Button>
    </li>
    );
};

export default ProgressionListRow;
