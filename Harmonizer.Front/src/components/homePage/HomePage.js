import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
//import { browserHistory } from 'react-router';
import { Button, Jumbotron } from 'react-bootstrap';
import { push } from 'react-router-redux';

class HomePage extends Component {
  render() {
    return (
        <Jumbotron >
          <h1 className="display-3">Welcome to harmonizer</h1>
          <p>Create and share chord progressions easily !</p>
          <hr className="my-4" />
          <Button bsSize="large" bsStyle="primary" onClick={ this.props.goToProgression }>Create your own progression</Button>
          <span>{ " or " }</span>
          <Link to="browse">browse existing progressions</Link>
        </Jumbotron>
      );
  }
}
const mapStateToProps = (state) => {
  return {
    location: state.location
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    goToProgression: () => {
      dispatch(push('/progression'))
    }
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
