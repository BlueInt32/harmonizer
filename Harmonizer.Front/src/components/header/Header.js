import React from 'react';
import { Link } from 'react-router-dom';
import Loader from '../common/Loader';
//import EditableTitle from './EditableTitle';

const Header = ({loading, saveProgression, title}) => {

            /*<EditableTitle text={title} clickEditHandler={() => {}} />*/
  return (
      <nav className="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
      <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <Link to="/" className="navbar-brand">Harmonizer <Loader></Loader></Link>
      <div className="collapse navbar-collapse" id="navbarCollapse">
        <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link to="/browse" className="nav-link">Browse</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/progression">New</Link>
            </li>
          <li className="nav-item">
            <a className="nav-link disabled">Disabled</a>
          </li>
        </ul>
        <form className="form-inline mt-2 mt-md-0">
          <input className="form-control mr-sm-2" type="text" placeholder="Search"/>
          <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
    );
}
export default Header;
