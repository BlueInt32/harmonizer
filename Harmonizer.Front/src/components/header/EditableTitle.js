import React, { Component }from 'react';
import { IndexLink } from 'react-router';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';

class EditableTitle extends Component {
  constructor (props, context){
    super(props, context);
    this.editMode = 'off';

    this.state = {
      editMode: 'off',
      text: props.text
    };

    // bindings
    this.toggleTitleEdition = this.toggleTitleEdition.bind(this);
    this.changeText = this.changeText.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if(newProps.text !== this.state.text){
      this.setState({text: newProps.text});
    }
  }

  toggleTitleEdition() {
    console.log(this.editMode);
    if(this.state.editMode === 'off') {
      console.log('swich to on');
      this.setState({editMode:'on', text:''});
      this.nameInput.focus();
    } else {

    }
  }

  changeText(event) {
    this.setState({text:event.target.value})
  }

  render () {
    let titleLink = null;
    let editLink = null;
    if(this.state.editMode === 'off') {
      titleLink = <IndexLink to="/" className="mainTitle">H - {this.state.text}</IndexLink>;
    }
    else {
      titleLink = <input ref={(input)=> { this.nameInput = input; }} className="" type="text" value={this.state.text} onChange={this.changeText.bind(this)}/>
    }
    editLink = <Button flat icon="mode_edit" onClick={this.toggleTitleEdition.bind(null)}/>
    return (
      <div>
        {titleLink}{editLink}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {state, ownProps};
};
const mapDispatchToProps = (dispatch) => {
  return { };
}
export default connect()(EditableTitle);
