progressionsList [
  title
  description
  thumbnail
]
progressionContext
  id
  title
  description
  chords [
    positionInSequence
    chordId,
    isPlaying,
    chord
      rootNoteId,
      enharmonicNoteId
      chordTypeId,
      valueId
      notation
  ]
  editedChord
    chordId
    chordNotation
    commitPosition
    rootNoteId
    enharmonicNoteId
    chordTypeId
    valueId
    notation
staticData 
  defaultNoteId
  defaultTypeId
  defaultValueId
  chords []
playerData
  bool isPlaying
  playerSegments []
    string rootNoteId
    string chordTypeId
    int valueId
appState
  string page
  bool isEditingTitle
  bool isEditingSettings
  bool isPlayingProgression
  bool isPlayingSingleChord
  bool isProgressionDirty
  int numAjaxCallsInProgress

