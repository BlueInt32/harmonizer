manageProgressionPage
  startEditingTitle(previousValue): 
    appState.isEditingTitle -> true
  commitEditingTitle(newValue): 
    appState.isEditingTitle -> false, 
    appState.isProgressionDirty -> true
    data.progressionMetaData.title -> newValue
  cancelEditingTitle: 
    appState.isEditingTitle -> false

  startEditingDescription(previousValue): 
    appState.isEditingDescription -> true
  commitEditingDescription(newValue): 
    appState.isEditingDescription -> false, 
    appState.isProgressionDirty -> true
    data.progressionMetaData.title -> newValue
  cancelEditingDescription: 
    appState.isEditingDescription -> false
	
  startAddingChord(): 
    appState.isEditingChord -> true, 
    data.editedChord.rootNoteId -> defaultNoteId, 
    data.editedChord.typeId -> defaultTypeId, 
    data.editedChord.durationId -> defaultDurationId, 
    data.commitPosition -> chords.length

  startEditingChord(chord): 
    appState.isEditingChord -> true, 
    data.editedChord.noteId -> chord.rootNoteId, 
    data.editedChord.durationId -> chord.durationId, 
    data.editedChord.typeId -> chord.chordTypeId, 
    data.commitPosition -> chord.positionInSequence

  changeNote(newNoteId):
    data.editedChord.noteId -> newNoteId

  changeChordType(newChordTypeId):
    data.editedChord.typeId -> newChordTypeId

  changeDuration(newDurationId):
    data.editedChord.durationId -> newDurationId

  commitChord(): 
    appState.isEditingChord -> false
    appState.isProgressionDirty -> true
    data.chords[commitPosition] -> { 
      findChordInStaticData(data.editedChord.noteId, data.editedChord.durationId, data.editedChord.chordType
    )}
  cancelEditChord():
    appState.isEditingChord -> false

  saveProgression():
    numAjaxCallsInProgress+=1
  ~ProgressionSavedSuccess()
    numAjaxCallsInProgress-=1
    appState.isProgressionDirty -> false
  ~ProgressionSavedFailure()
    numAjaxCallsInProgress-=1
    appState.isProgressionDirty -> true

  playProgression(): 
    appState.playingProgression -> true

  ~endProgression():
    appState.playingProgression -> false
