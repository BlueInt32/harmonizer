﻿using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Harmonizer.Domain.Entities.ServiceArgs;
using Harmonizer.Domain.Interfaces;

namespace Harmonizer.Repository
{
    public class ProgressionChordRepository : IProgressionChordRepository
    {
        ProgressionChordRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        private readonly string _connectionString;

        public void CreateProgressionChord(ProgressionBarToSaveArgs progressionChord)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string sql = @"insert into ProgressionChords values (@PositionInProgression, @ChordId, @ProgressionId);
                                select cast(SCOPE_IDENTITY() as int)";
                var id = connection.Query<int>(sql, progressionChord).Single();
            }
        }

        public void DeleteProgressionChord(int progressionChordId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string sql = @" delete from ProgressionChords where Id = @id";
                connection.Execute(sql, new { id = progressionChordId });
            }
        }
    }
}
