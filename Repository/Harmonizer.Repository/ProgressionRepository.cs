﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Entities.Exceptions;
using Harmonizer.Domain.Entities.ServiceArgs;
using Harmonizer.Domain.Interfaces;

namespace Harmonizer.Repository
{
    public class ProgressionRepository : IProgressionRepository
    {
        public ProgressionRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        private readonly string _connectionString;

        public int CreateProgression(ProgressionToSaveArgs progressionToSaveArgs)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // 1. Check consistency of bars and chords
                var chordsIds = progressionToSaveArgs.BarsChordsId.SelectMany(l => l.Select(m => m)).ToList();
                string sqlCheck = @"select Id, ValueId from Chords where Id in @ids";
                var foundChords = connection.Query<Chord>(sqlCheck, new { ids = chordsIds });
                if (chordsIds.Count > foundChords.Count())
                {
                    throw new ChordNotFoundException(chordsIds);
                }

                //throw new Exception("Must validate that chords values and progression time signature are consistent"); 

                // 2. Procede to save
                string sql = @"insert into Progressions values (@Title, @Description, @TempoId);
                                select cast(SCOPE_IDENTITY() as int)";
                var progressionGeneratedId = connection.Query<int>(sql, progressionToSaveArgs).Single();

                var barsList = progressionToSaveArgs.BarsChordsId?.ToList();
                for (int i = 0; i < barsList?.Count; i++)
                {
                    var progressionBar = new ProgressionBarToSaveArgs
                    {
                        PositionInProgression = i,
                        ProgressionId = progressionGeneratedId
                    };
                    string progressionBarSql = @"insert into ProgressionBars values (@PositionInProgression, @ProgressionId);
                                select cast(SCOPE_IDENTITY() as int)";
                    var generatedBarId = connection.Query<int>(progressionBarSql, progressionBar).Single();

                    var chordsListInBar = barsList[i].ToList();

                    for (int j = 0; j < chordsListInBar.Count; j++)
                    {
                        var barChord = new BarChordToSaveArgs
                        {
                            PositionInBar = j,
                            ChordId = chordsListInBar[j],
                            BarId = generatedBarId
                        };
                        string barChordSql = @"insert into BarChords values (@PositionInBar, @ChordId, @BarId);
                                select cast(SCOPE_IDENTITY() as int)";
                        var generatedBarChordId = connection.Query<int>(barChordSql, barChord).Single();
                    }
                }

                return progressionGeneratedId;
            }
        }

        public void DeleteProgression(int progressionId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string sql = @"delete from Progressions where Id = @id";
                connection.Execute(sql, new { id = progressionId });
            }
        }

        public Progression GetProgression(int progressionId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string sql = @"select 
                        p.Id,
                        p.Title,
                        p.Description,
                        p.TempoId,
                        pb.Id,
                        pb.ProgressionId,
                        pb.PositionInProgression,
                        bc.Id,
                        bc.PositionInBar,
                        bc.ChordId
                        from Progressions p
                        inner join ProgressionBars pb on pb.ProgressionId = p.Id
                        inner join BarChords bc on pb.Id = bc.BarId
                        where p.Id = @id
                        order by pb.PositionInProgression asc, bc.PositionInBar asc";

                var lookup = new Dictionary<int, Progression>();
                var lastBarId = 0;
                connection.Query<Progression, ProgressionBar, BarChord, Progression>(sql, (p, pb, bc) =>
                {
                    Progression progression;
                    if (!lookup.TryGetValue(p.Id, out progression))
                    {
                        progression = p;
                        lookup.Add(p.Id, p);
                    }
                    if (progression.BarsChordsIds == null)
                    {
                        progression.BarsChordsIds = new List<List<int>>();
                    }

                    if (pb.Id != lastBarId)
                    {
                        progression.BarsChordsIds.Add(new List<int> { bc.ChordId });
                    }
                    else
                    {
                        progression.BarsChordsIds[progression.BarsChordsIds.Count - 1].Add(bc.ChordId);
                    }
                    lastBarId = pb.Id;
                    return progression;
                }, new { id = progressionId })
                    .AsQueryable();
                //Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(Progression).GetTypeInfo(), new List<string> { "Id" });

                //var mappedProgression = (Slapper.AutoMapper.MapDynamic<Progression>(progressions) as IEnumerable<Progression>).FirstOrDefault();

                var mappedProgression = lookup.Values;
                if (mappedProgression.Count == 0)
                {
                    throw new ProgressionNotFoundException(progressionId);
                }

                return mappedProgression.First();
            }
        }

        public void CreateOrUpdateProgression(ProgressionToSaveArgs progression)
        {
            if (!progression.Id.HasValue)
            {
                CreateProgression(progression);
                return;
            }
            var progressionInDb = GetProgression(progression.Id.Value);
            if (progressionInDb != null)
            {
                UpdateProgression(progression);
            }
            else
            {
                CreateProgression(progression);
            }
        }

        public List<Progression> FindProgressions(SearchProgressionQuery query)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                Func<string, string> encodeForLike = input => input.Replace("[", "[[]").Replace("%", "[%]");

                string term = "%" + encodeForLike(query.Term) + "%";

                var lookup = new Dictionary<int, Progression>();
                string sql = @"select 
                        p.Id,
                        p.Title,
                        p.Description,
                        p.TempoId,
                        pb.Id,
                        pb.ProgressionId,
                        pb.PositionInProgression,
                        bc.Id,
                        bc.PositionInBar,
                        bc.ChordId
                        from Progressions p
                        inner join ProgressionBars pb on pb.ProgressionId = p.Id
                        inner join BarChords bc on pb.Id = bc.BarId
                        where p.Title like @term
                        order by p.Id asc, pb.PositionInProgression asc, bc.PositionInBar asc";
                var lastBarId = 0;
                connection.Query<Progression, ProgressionBar, BarChord, Progression>(sql, (p, pb, bc) =>
                {
                    Progression progression;
                    if (!lookup.TryGetValue(p.Id, out progression))
                    {
                        progression = p;
                        lookup.Add(p.Id, p);
                        lastBarId = 0;
                    }
                    if (progression.BarsChordsIds == null)
                    {
                        progression.BarsChordsIds = new List<List<int>>();
                    }

                    if (pb.Id != lastBarId)
                    {
                        progression.BarsChordsIds.Add(new List<int> { bc.ChordId });
                    }
                    else
                    {
                        progression.BarsChordsIds[progression.BarsChordsIds.Count - 1].Add(bc.ChordId);
                    }
                    lastBarId = pb.Id;
                    return progression;
                }, new { term = term });
                //    .AsQueryable();
                //connection.Query<Progression, ProgressionChord, Progression>(sql, (s, sc) =>
                //    {
                //        Progression progression;
                //        if (!lookup.TryGetValue(s.Id, out progression))
                //        {
                //            lookup.Add(s.Id, progression = s);
                //        }
                //        if (progression.BarsChordsIds == null)
                //            progression.BarsChordsIds = new List<int>();
                //        progression.BarsChordsIds.Add(sc.ChordId);
                //        return progression;
                //    }, new { term = term }, splitOn: "ChordId").AsQueryable();

                var mappedProgressions = lookup.Values
                    .Skip((query.Pagination.PageNumber - 1) * query.Pagination.PageSize)
                    .Take(query.Pagination.PageSize)
                    .ToList();
                return mappedProgressions;
            }
        }

        public void UpdateProgression(ProgressionToSaveArgs progressionToSaveArgs)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // 1. Replace base progression data
                string sql = @"update Progressions set title=@Title, description=@description, tempoId=@TempoId
                                where id = @id;";
                connection.Query<int>(sql, progressionToSaveArgs);

                // TODO: check if chords replacement can be avoided
                // 2. delete associated chords
                string deletionSql = @"delete from ProgressionBars where ProgressionId=@Id";
                var deleteResult = connection.Query(deletionSql, progressionToSaveArgs);

                // 3. add new chords
                var barsList = progressionToSaveArgs.BarsChordsId?.ToList();
                for (int i = 0; i < barsList?.Count; i++)
                {
                    var progressionBar = new ProgressionBarToSaveArgs
                    {
                        PositionInProgression = i,
                        ProgressionId = progressionToSaveArgs.Id.Value
                    };
                    string progressionBarSql = @"insert into ProgressionBars values (@PositionInProgression, @ProgressionId);
                                select cast(SCOPE_IDENTITY() as int)";
                    var generatedBarId = connection.Query<int>(progressionBarSql, progressionBar).Single();

                    var chordsListInBar = barsList[i].ToList();

                    for (int j = 0; j < chordsListInBar.Count; j++)
                    {
                        var barChord = new BarChordToSaveArgs
                        {
                            PositionInBar = j,
                            ChordId = chordsListInBar[j],
                            BarId = generatedBarId
                        };
                        string barChordSql = @"insert into BarChords values (@PositionInBar, @ChordId, @BarId);
                                select cast(SCOPE_IDENTITY() as int)";
                        var generatedBarChordId = connection.Query<int>(barChordSql, barChord).Single();
                    }
                }
            }
        }
    }
}