﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Interfaces;

namespace Harmonizer.Repository
{
    public class StaticDataRepository : IStaticDataRepository
    {
        public StaticDataRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        private readonly string _connectionString;
        public List<Chord> GetChords()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                IEnumerable<Chord> chords = connection.Query<Chord>("select * from Chords");
                return chords.ToList();
            }
        }
        public List<ChordType> GetChordTypes()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                IEnumerable<ChordType> chordTypes = connection.Query<ChordType>("select * from ChordTypes order by [Order]");
                return chordTypes.ToList();
            }
        }
        public List<Note> GetNotes()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                IEnumerable<Note> notes = connection.Query<Note>("select * from Notes order by [Order]");
                return notes.ToList();
            }
        }
        public List<Tempo> GetTempos()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                IEnumerable<Tempo> tempos = connection.Query<Tempo>("select * from Tempos");
                return tempos.ToList();
            }
        }

        public List<Value> GetValues()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                IEnumerable<Value> values = connection.Query<Value>("select * from [Values]");
                return values.ToList();
            }
        }
        public List<Configuration> GetConfigurations()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                IEnumerable<Configuration> configurations = connection.Query<Configuration>("select * from Configurations");
                return configurations.ToList();
            }
        }
    }
}
