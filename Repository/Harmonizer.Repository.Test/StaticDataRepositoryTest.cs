﻿using System.IO;
using Harmonizer.Domain.Interfaces;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Harmonizer.Repository.Test
{

    [TestFixture]
    public class StaticDataTest
    {
        private IStaticDataRepository _staticDataRepository;
            //new StaticDataRepository(ConfigurationManager.ConnectionStrings["HarmonizerContext"].ConnectionString);

        [SetUp]
        public void Setup()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json");

            IConfigurationRoot config = builder.Build();
            var connectionString = config.GetConnectionString("Harmonizer");
            _staticDataRepository = new StaticDataRepository(connectionString);
        }

        [Test]
        public void TestStaticData()
        {
            // Arrange

            // Act
            var tempos = _staticDataRepository.GetTempos();
            var notes = _staticDataRepository.GetNotes();
            var values = _staticDataRepository.GetValues();
            var chordTypes = _staticDataRepository.GetChordTypes();
            var chords = _staticDataRepository.GetChords();

            // Assert
            Assert.IsNotNull(chords);
            Assert.IsTrue(chords.Count > 1);
        }
    }
}
