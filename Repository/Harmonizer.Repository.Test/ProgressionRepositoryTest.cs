﻿using System.Collections.Generic;
using System.IO;
using Harmonizer.Domain.Entities;
using Harmonizer.Domain.Entities.Exceptions;
using Harmonizer.Domain.Entities.ServiceArgs;
using Harmonizer.Domain.Interfaces;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Harmonizer.Repository.Test
{
    //[TestFixture]
    public class ProgressionTest
    {
        private IProgressionRepository _progressionRepository;

        [SetUp]
        public void Setup()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json");

            IConfigurationRoot config = builder.Build();
            var connectionString = config.GetConnectionString("Harmonizer");
            _progressionRepository = new ProgressionRepository(connectionString);
        }
        [Test]
        public void CreateProgression_ShouldReturnId()
        {
            // arrange
            ProgressionToSaveArgs s = new ProgressionToSaveArgs
            {
                Title = "ProgressionTest_",
                Description = "bar",
                TempoId = 70,
                BarsChordsId = new List<List<int>> { new List<int> { 1, 25 }, new List<int> { 50 } }
            };

            // act
            int progressionId = _progressionRepository.CreateProgression(s);
            Progression sRead = _progressionRepository.GetProgression(progressionId);

            // assert
            Assert.IsTrue(progressionId > 0);
        }


        [Test]
        public void CreateProgression_ShouldSaveCorrectData()
        {
            // arrange
            ProgressionToSaveArgs s = new ProgressionToSaveArgs
            {
                Title = "ProgressionTest_SaveAndGetProgression",
                Description = "So good !",
                TempoId = 70,
                BarsChordsId = new List<List<int>> { new List<int> { 1, 25 }, new List<int> { 50 } }
            };

            // act
            int progressionId = _progressionRepository.CreateProgression(s);
            Progression sRead = _progressionRepository.GetProgression(progressionId);

            // assert
            Assert.IsTrue(progressionId > 0);
            Assert.IsNotNull(sRead);
            Assert.AreEqual("ProgressionTest_SaveAndGetProgression", sRead.Title);
            Assert.AreEqual("So good !", sRead.Description);
            Assert.AreEqual(70, sRead.TempoId);
            Assert.AreEqual(3, sRead.BarsChordsIds.Count);
            Assert.AreEqual(25, sRead.BarsChordsIds[1]);
            Assert.AreEqual(50, sRead.BarsChordsIds[2]);
        }

        [Test]
        public void DeleteProgression_ProgressionShouldNotBeFoundAfterDelete()
        {
            // arrange
            ProgressionToSaveArgs s = new ProgressionToSaveArgs
            {
                Title = "ProgressionTest_SaveAndGetProgression",
                Description = "So good !",
                TempoId = 70,
                BarsChordsId = new List<List<int>> { new List<int> { 1, 25 }, new List<int> { 50 } }
            };
            int progressionId = _progressionRepository.CreateProgression(s);

            // act
            _progressionRepository.DeleteProgression(progressionId);

            // assert
            Assert.Throws<ProgressionNotFoundException>(() => _progressionRepository.GetProgression(progressionId));
        }

        [Test]
        public void DeleteProgression_ShouldThrowIfProgressionDoesNotExist()
        {
            // arrange

            // act
            _progressionRepository.DeleteProgression(-1);

            // assert no throw
        }

        [Test]
        public void Search_ShouldFindCorrespondingProgressions()
        {
            var s1 = new ProgressionToSaveArgs { Title = "ProgressionTest_SearchMethod_1", Description = "So good !", TempoId = 70 };
            s1.BarsChordsId = new List<List<int>> { new List<int> { 1, 25 }, new List<int> { 50 } };

            var s2 = new ProgressionToSaveArgs { Title = "ProgressionTest_SearchMethod_1", Description = "So bad !", TempoId = 70 };

            _progressionRepository.CreateProgression(s1);
            _progressionRepository.CreateProgression(s2);

            var progressions = _progressionRepository.FindProgressions(new SearchProgressionQuery("ProgressionTest_SearchMethod"));

            Assert.AreEqual(2, progressions.Count, "Search by name");
        }
        [Test]
        public void Search_ShouldMapProgressionChords()
        {
            var s1 = new ProgressionToSaveArgs { Title = "ProgressionTest_SearchMethod_1", Description = "So good !", TempoId = 70 };
            s1.BarsChordsId = new List<List<int>> { new List<int> { 1, 25 }, new List<int> { 50 } };

            _progressionRepository.CreateProgression(s1);

            var progressions = _progressionRepository.FindProgressions(new SearchProgressionQuery("ProgressionTest_SearchMethod"));

            Assert.AreEqual(52, progressions[0].BarsChordsIds[1], "Search by name");
        }

        [TearDown]
        public void CleanUp()
        {
            //CallContext.FreeNamedDataSlot("__Key");
            var progressions = _progressionRepository.FindProgressions(new SearchProgressionQuery("ProgressionTest_"));
            foreach (Progression progression in progressions)
            {
                _progressionRepository.DeleteProgression(progression.Id);
            }
        }

        [Test, Category("Exception scenario")]
        public void GetProgression_ShouldThrowNotFoundExceptionWhenNoProgressionExist()
        {
            Assert.Throws<ProgressionNotFoundException>(() => _progressionRepository.GetProgression(-1));
        }
    }
}
